package com.mgrmobi.intouch.cameramodule.editor.ui.emoji;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public class EmojiRecyclerView extends RecyclerView {

    public EmojiRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }
}
