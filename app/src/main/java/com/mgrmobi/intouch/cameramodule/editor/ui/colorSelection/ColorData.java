package com.mgrmobi.intouch.cameramodule.editor.ui.colorSelection;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorInt;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */
public final class ColorData implements Parcelable {
    @ColorInt public final int color;
    public boolean isChecked;

    private ColorData(@ColorInt int color) {
        this.color = color;
    }

    public static ColorData of(@ColorInt int color) {
        return new ColorData(color);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ColorData colorData = (ColorData) o;

        return color == colorData.color;

    }

    @Override
    public int hashCode() {
        return color;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.color);
        dest.writeByte(this.isChecked ? (byte) 1 : (byte) 0);
    }

    protected ColorData(Parcel in) {
        this.color = in.readInt();
        this.isChecked = in.readByte() != 0;
    }

    public static final Parcelable.Creator<ColorData> CREATOR = new Parcelable.Creator<ColorData>() {
        @Override
        public ColorData createFromParcel(Parcel source) {
            return new ColorData(source);
        }

        @Override
        public ColorData[] newArray(int size) {
            return new ColorData[size];
        }
    };
}
