package com.mgrmobi.intouch.cameramodule.camera.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.*;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.mgrmobi.intouch.cameramodule.editor.ui.colorSelection.WidgetSimpleColorSelector;
import com.mgrmobi.intouch.cameramodule.editor.ui.drawing.FreeDrawView;
import com.mgrmobi.intouch.cameramodule.editor.ui.drawing.PathDrawnListener;
import com.mgrmobi.intouch.cameramodule.editor.ui.emoji.AdapterStickerList;
import com.mgrmobi.intouch.cameramodule.editor.ui.emoji.EmojiRecyclerView;
import com.mgrmobi.intouch.cameramodule.editor.ui.text.TextEditorDialogFragment;
import com.mgrmobi.intouch.cameramodule.editor.ui.widget.StateTextButton;
import com.mgrmobi.intouch.cameramodule.editor.ui.widget.TwoStateImageButton;
import com.mgrmobi.intouch.cameramodule.editor.utils.UiHelper;
import com.mgrmobi.intouch.cameramodule.editor.viewmodel.Font;
import com.mgrmobi.intouch.cameramodule.editor.viewmodel.ImageLayer;
import com.mgrmobi.intouch.cameramodule.editor.viewmodel.Layer;
import com.mgrmobi.intouch.cameramodule.editor.viewmodel.TextLayer;
import com.mgrmobi.intouch.cameramodule.editor.widget.ButtonDone;
import com.mgrmobi.intouch.cameramodule.editor.widget.MotionView;
import com.mgrmobi.intouch.cameramodule.editor.widget.entity.ImageEntity;
import com.mgrmobi.intouch.cameramodule.editor.widget.entity.MotionEntity;
import com.mgrmobi.intouch.cameramodule.editor.widget.entity.TextEntity;
import intouch.mgrmobi.com.intouchcameramodule.R;

public class ActivityPickedPhotoPreview extends AppCompatActivity implements
        TextEditorDialogFragment.OnTextLayerCallback,
        AdapterStickerList.OnStickerClickListener,
        PreviewRootLayout.EmojiListListener,
        PathDrawnListener {
    private static final int NO_VALUE = Integer.MIN_VALUE;
    private final RectF buttonRemoveBounds = new RectF();
    @Bind(R.id.preview_root) PreviewRootLayout previewRootLayout;
    @Bind(R.id.captured_image) ImageView imageView;
    @Bind(R.id.motion_view) MotionView motionView;
    @Bind(R.id.button_done) ButtonDone buttonDone;
    @Bind(R.id.button_retake) StateTextButton stateTextButton;
    @Bind(R.id.button_add_text) View buttonAddText;
    @Bind(R.id.button_add_drawing) TwoStateImageButton buttonAddDrawing;
    @Bind(R.id.button_add_emoji) TwoStateImageButton buttonAddEmoji;
    @Bind(R.id.emoji_list) EmojiRecyclerView emojiList;
    @Bind(R.id.drawing_view) FreeDrawView drawView;
    @Bind(R.id.widget_color_picker) WidgetSimpleColorSelector colorPicker;
    private AnimatorSet animatorEmojiesListAppearing;
    private AnimatorSet animatorEmojiesListDisappearing;
    private AnimatorSet animatorDrawingAppearing;
    private AnimatorSet animatorDrawingDisappearing;
    private TextEditorDialogFragment enterTextFragment;

    private final MotionView.MotionViewCallback motionViewCallback = new MotionView.MotionViewCallback() {
        @Override
        public void onEntitySelected(@Nullable MotionEntity entity) {

        }

        @Override
        public void onEntityDoubleTap(@NonNull MotionEntity entity) {
            if (entity instanceof TextEntity) {
                startTextEntityEditing();
            }
        }
    };

    public static Intent createIntent(Context callingContext) {
        return new Intent(callingContext, ActivityPickedPhotoPreview.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getWindow().setWindowAnimations(R.style.NoAnimation);
        super.onCreate(savedInstanceState);
        Bitmap bitmap = ResultHolder.getImage();
        if (bitmap == null) {
            finish();
            return;
        }

        setContentView(R.layout.activity_preview);
        ButterKnife.bind(this);

        imageView.setImageBitmap(bitmap);

        previewRootLayout.setEmojiesListListener(this);
        previewRootLayout.setOnStickerClickListener(this);

        motionView.setMotionViewCallback(motionViewCallback);

        motionView.setTouchCallback(new MotionView.TouchCallback() {
            private final float slop = ViewConfiguration.get(ActivityPickedPhotoPreview.this).getScaledTouchSlop();
            private float initialX;
            private float initialY;
            private boolean contentHidden;

            @Override
            public void onPointerDown(@Nullable MotionEntity motionEntity, float touchX, float touchY) {
                initialX = touchX;
                initialY = touchY;
            }

            @Override
            public void onPointerMoved(@Nullable MotionEntity motionEntity, float touchX, float touchY) {
                if (motionEntity != null) {
                    if (!contentHidden) {
                        contentHidden = true;
                        hideControlsAfterPointerBeganMovement();
                    }
                    if (buttonRemoveBounds.contains(touchX, touchY)) {
                        if (buttonDone != null) {
                            buttonDone.setPreparedToRemove(true);
                        }
                    } else {
                        if ((Math.abs(touchX - initialX) > slop || Math.abs(touchY - initialY) > slop) && buttonDone != null) {
                            buttonDone.switchToRemove();
                            buttonDone.setPreparedToRemove(false);
                        }
                    }
                }
            }

            @Override
            public void onPointerUp(@Nullable MotionEntity motionEntity, float touchX, float touchY) {
                if (motionEntity != null) {
                    if (buttonRemoveBounds.contains(touchX, touchY)) {
                        if (motionView != null) {
                            motionView.deleteSelectedEntity();
                        }
                    }
                }
                if (buttonDone != null) {
                    buttonDone.switchToDone();
                }
                if (contentHidden) {
                    contentHidden = false;
                    showControlsAfterPointerFinishedMovement();
                }
            }
        });
        UiHelper.waitForMeasuring(buttonDone, () -> {
            Rect rect = new Rect();
            buttonDone.getGlobalVisibleRect(rect);
            buttonRemoveBounds.set(rect);
        });

        drawView.setEnabled(false);

        buttonAddDrawing.setStateClickListener(new TwoStateImageButton.StateClickListener() {

            @Override
            public void onDefaultStateClicked(View v) {
                if (drawView != null) {
                    if (!drawView.isEnabled()) {
                        startDrawing();
                    }
                }
            }

            @Override
            public void onAlternateStateClicked(View v) {
                if (drawView != null) {
                    drawView.undoLast();
                }
            }
        });

        buttonAddEmoji.setStateClickListener(new TwoStateImageButton.StateClickListener() {
            @Override
            public void onDefaultStateClicked(View v) {
                if (emojiList != null && previewRootLayout != null) {
                    previewRootLayout.showOrHideEmojiesList();
                }
            }

            @Override
            public void onAlternateStateClicked(View v) {
                finishDrawing();
            }
        });
        colorPicker.setColorSelectionListener(color -> {
            if (drawView != null) {
                drawView.setPaintColor(color);
            }
        });
        drawView.setOnPathDrawnListener(this);
    }

    @OnClick(R.id.button_retake)
    void onRetakeButtonClicked() {
        if (drawView.isEnabled()) {
            drawView.undoAll();
        } else {
            onBackPressed();
        }
    }

    @OnClick(R.id.button_add_text)
    void onAddTextButtonClicked() {
        if (previewRootLayout != null) {
            previewRootLayout.hideEmojiesList(false);
        }
        addTextSticker();
    }

    @OnClick(R.id.button_done)
    void onButtonDoneClicked() {

    }

    @Nullable
    private TextEntity currentTextEntity() {
        if (motionView != null && motionView.getSelectedEntity() instanceof TextEntity) {
            return ((TextEntity) motionView.getSelectedEntity());
        } else {
            return null;
        }
    }

    private void startTextEntityEditing() {
        hideControlsBeforeTextEditing();
        TextEntity entity = currentTextEntity();
        if (entity != null) {
            showEnterTextDialog(entity.getLayer().getText(), entity.getLayer().getFont().getColor());
            if (motionView.setSelectedEntityVisible(false)) {
                motionView.invalidate();
            }
        } else {
            showEnterTextDialog(null, NO_VALUE);
        }
    }

    protected void addTextSticker() {
        if (motionView != null) {
            motionView.unselectEntity();

            hideControlsBeforeTextEditing();
            showEnterTextDialog(null, NO_VALUE);
        }
    }

    private void showEnterTextDialog(@Nullable String textToEdit, @ColorInt int textColor) {
        if (enterTextFragment == null) {
            enterTextFragment = TextEditorDialogFragment.getInstance();
        }
        enterTextFragment.setTextAndColor(textToEdit, textColor);
        enterTextFragment.show(getSupportFragmentManager(), TextEditorDialogFragment.class.getName());
    }

    @Override
    public void onTextEntered(@NonNull String text, @ColorInt int selectedColor) {
        if (text.isEmpty()) {
            return;
        }
        if (currentTextEntity() == null) {
            TextLayer textLayer = createTextLayer(text, selectedColor);
            TextEntity textEntity = new TextEntity(textLayer, motionView.getWidth(), motionView.getHeight(), null);
            motionView.addEntityAndPosition(textEntity);

            moveNewlyCreatedEntityToTop(textEntity);

            motionView.invalidate();
        } else {
            textChanged(text, selectedColor);
        }
    }

    private TextLayer createTextLayer(@NonNull String text, @ColorInt int color) {
        TextLayer textLayer = new TextLayer();
        Font font = new Font();

        font.setColor(color);
        font.setSize(TextLayer.Limits.INITIAL_FONT_SIZE);

        textLayer.setFont(font);
        textLayer.setText(text);

        return textLayer;
    }

    private void textChanged(@NonNull String text, @ColorInt int selectedColor) {
        motionView.setSelectedEntityVisible(true);

        if (!text.isEmpty()) {
            TextEntity textEntity = currentTextEntity();
            if (textEntity != null) {
                TextLayer textLayer = textEntity.getLayer();
                if (!text.equals(textLayer.getText())) {
                    if (textLayer.getFont().getColor() != selectedColor) {
                        textLayer.getFont().setColor(selectedColor);
                    }
                    textLayer.setText(text);
                    textEntity.updateEntity();
                    motionView.invalidate();
                } else if (textLayer.getFont().getColor() != selectedColor) {
                    textLayer.getFont().setColor(selectedColor);
                    textEntity.updateEntity();
                    motionView.invalidate();
                }
            }
        }
    }

    @Override
    public void onStickerClicked(@DrawableRes int resourceId) {
        motionView.post(() -> {
            Layer layer = new ImageLayer();
            Bitmap sticker = BitmapFactory.decodeResource(getResources(), resourceId);
            ImageEntity imageEntity = new ImageEntity(layer, sticker, motionView.getWidth(), motionView.getHeight());
            motionView.addEntityAndPosition(imageEntity);
            moveNewlyCreatedEntityToTop(imageEntity);

            motionView.invalidate();
        });

        if (previewRootLayout != null) {
            previewRootLayout.hideEmojiesList();
        }
    }

    // move text sticker up so that its not hidden under keyboard
    private void moveNewlyCreatedEntityToTop(@NonNull MotionEntity entity) {
        PointF center = entity.absoluteCenter();
        center.y *= 0.5F;
        entity.moveCenterTo(center);
    }

    @Override
    public void onEmojiesListVisible() {
        if (motionView != null) {
            motionView.unselectEntity();
        }
        if (animatorEmojiesListAppearing == null) {
            animatorEmojiesListAppearing = new AnimatorSet();
            animatorEmojiesListAppearing.playTogether(
                    ObjectAnimator.ofFloat(buttonAddText, View.SCALE_X, 0.5f),
                    ObjectAnimator.ofFloat(buttonAddText, View.SCALE_Y, 0.5f),
                    ObjectAnimator.ofFloat(buttonAddText, View.ALPHA, 0.2f),
                    ObjectAnimator.ofFloat(buttonAddDrawing, View.SCALE_X, 0.5f),
                    ObjectAnimator.ofFloat(buttonAddDrawing, View.SCALE_Y, 0.5f),
                    ObjectAnimator.ofFloat(buttonAddDrawing, View.ALPHA, 0.2f)
            );
            animatorEmojiesListAppearing.setDuration(120);
            animatorEmojiesListAppearing.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationCancel(Animator animation) {
                    UiHelper.invisible(buttonAddText, buttonAddDrawing);
                    UiHelper.disable(buttonAddText, buttonAddDrawing);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    UiHelper.invisible(buttonAddText, buttonAddDrawing);
                    UiHelper.disable(buttonAddText, buttonAddDrawing);
                }
            });
            animatorEmojiesListAppearing.setInterpolator(new FastOutSlowInInterpolator());
        }
        if (animatorEmojiesListDisappearing != null) {
            animatorEmojiesListDisappearing.cancel();
        }
        stateTextButton.setState(StateTextButton.STATE_EMOJIES);
        animatorEmojiesListAppearing.start();
        buttonAddEmoji.animate().scaleX(1.4f).scaleY(1.4f).withLayer().setDuration(120).setInterpolator(new OvershootInterpolator(2));
    }

    @Override
    public void onEmojiesListHidden() {
        UiHelper.visible(buttonAddText, buttonAddDrawing);
        if (animatorEmojiesListDisappearing == null) {
            animatorEmojiesListDisappearing = new AnimatorSet();
            animatorEmojiesListDisappearing.playTogether(
                    ObjectAnimator.ofFloat(buttonAddText, View.SCALE_X, 1f),
                    ObjectAnimator.ofFloat(buttonAddText, View.SCALE_Y, 1f),
                    ObjectAnimator.ofFloat(buttonAddText, View.ALPHA, 1f),
                    ObjectAnimator.ofFloat(buttonAddDrawing, View.SCALE_X, 1f),
                    ObjectAnimator.ofFloat(buttonAddDrawing, View.SCALE_Y, 1f),
                    ObjectAnimator.ofFloat(buttonAddDrawing, View.ALPHA, 1f)
            );
            animatorEmojiesListDisappearing.setDuration(120);
            animatorEmojiesListDisappearing.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationCancel(Animator animation) {
                    UiHelper.enable(buttonAddText, buttonAddDrawing);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    UiHelper.enable(buttonAddText, buttonAddDrawing);
                }
            });
            animatorEmojiesListDisappearing.setInterpolator(new FastOutSlowInInterpolator());
        }
        if (animatorEmojiesListAppearing != null) {
            animatorEmojiesListAppearing.cancel();
        }
        stateTextButton.setState(StateTextButton.STATE_DEFAULT);
        animatorEmojiesListDisappearing.start();
        buttonAddEmoji.animate().scaleX(1f).scaleY(1f).withLayer().setDuration(120).setInterpolator(new OvershootInterpolator(2));
    }

    @Override
    public void onDismissed() {
        showControlsAfterTextEditing();
        if (motionView.setSelectedEntityVisible(true)) {
            motionView.invalidate();
        }
    }

    private Transition fade;

    private void hideControlsBeforeTextEditing() {
        TransitionManager.beginDelayedTransition((ViewGroup) buttonAddDrawing.getParent(), ensureAppearingDisappearingTransition());
        UiHelper.gone(stateTextButton, buttonAddText, buttonAddDrawing, buttonAddEmoji);
    }

    private void showControlsAfterTextEditing() {
        TransitionManager.beginDelayedTransition((ViewGroup) buttonAddDrawing.getParent(), ensureAppearingDisappearingTransition());
        UiHelper.visible(stateTextButton, buttonAddText, buttonAddDrawing, buttonAddEmoji);
    }

    private void hideControlsAfterPointerBeganMovement() {
        TransitionManager.beginDelayedTransition((ViewGroup) buttonAddDrawing.getParent(), ensureAppearingDisappearingTransition());
        UiHelper.gone(stateTextButton, buttonAddText, buttonAddDrawing, buttonAddEmoji);
    }

    private void showControlsAfterPointerFinishedMovement() {
        TransitionManager.beginDelayedTransition((ViewGroup) buttonAddDrawing.getParent(), ensureAppearingDisappearingTransition());
        UiHelper.visible(stateTextButton, buttonAddText, buttonAddDrawing, buttonAddEmoji);
    }

    private void hideControlsAfterDrawingHasStarted() {
        TransitionManager.beginDelayedTransition((ViewGroup) buttonAddDrawing.getParent(), ensureAppearingDisappearingTransition());
        UiHelper.gone(stateTextButton, buttonAddDrawing, buttonAddEmoji);
    }

    private void showControlsAfterDrawingHasFinished() {
        TransitionManager.beginDelayedTransition((ViewGroup) buttonAddDrawing.getParent(), ensureAppearingDisappearingTransition());
        UiHelper.visible(stateTextButton, buttonAddDrawing, buttonAddEmoji);
    }

    private Transition ensureAppearingDisappearingTransition() {
        if (fade == null) {
            fade = new Fade()
                    .addTarget(stateTextButton)
                    .addTarget(buttonAddText)
                    .addTarget(buttonAddDrawing)
                    .addTarget(buttonAddEmoji)
                    .setDuration(100);
        }
        return fade;
    }

    private void startDrawing() {
        if (motionView != null) {
            motionView.unselectEntity();
        }
        if (animatorDrawingAppearing == null) {
            animatorDrawingAppearing = new AnimatorSet();
            animatorDrawingAppearing.playTogether(
                    ObjectAnimator.ofFloat(buttonAddText, View.SCALE_X, 0.5f),
                    ObjectAnimator.ofFloat(buttonAddText, View.SCALE_Y, 0.5f),
                    ObjectAnimator.ofFloat(buttonAddText, View.ALPHA, 0.2f)
            );
            animatorDrawingAppearing.setDuration(120);
            animatorDrawingAppearing.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationCancel(Animator animation) {
                    UiHelper.invisible(buttonAddText);
                    UiHelper.disable(buttonAddText, motionView);
                    UiHelper.enable(drawView);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    UiHelper.invisible(buttonAddText);
                    UiHelper.disable(buttonAddText, motionView);
                    UiHelper.enable(drawView);
                }
            });
            animatorDrawingAppearing.setInterpolator(new FastOutSlowInInterpolator());
        }
        if (animatorDrawingDisappearing != null) {
            animatorDrawingDisappearing.cancel();
        }
        stateTextButton.setState(StateTextButton.STATE_DRAWING);
        buttonAddDrawing.setState(TwoStateImageButton.STATE_ALTERNATIVE);
        buttonAddEmoji.setState(TwoStateImageButton.STATE_ALTERNATIVE);
        animatorDrawingAppearing.start();

        if (buttonDone != null) {
            buttonDone.hide();
        }

        UiHelper.visible(colorPicker);
    }

    private void finishDrawing() {
        UiHelper.visible(buttonAddText);
        if (animatorDrawingDisappearing == null) {
            animatorDrawingDisappearing = new AnimatorSet();
            animatorDrawingDisappearing.playTogether(
                    ObjectAnimator.ofFloat(buttonAddText, View.SCALE_X, 1f),
                    ObjectAnimator.ofFloat(buttonAddText, View.SCALE_Y, 1f),
                    ObjectAnimator.ofFloat(buttonAddText, View.ALPHA, 1f)
            );
            animatorDrawingDisappearing.setDuration(120);
            animatorDrawingDisappearing.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationCancel(Animator animation) {
                    UiHelper.enable(buttonAddText, motionView);
                    UiHelper.disable(drawView);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    UiHelper.enable(buttonAddText, motionView);
                    UiHelper.disable(drawView);
                }
            });
            animatorDrawingDisappearing.setInterpolator(new FastOutSlowInInterpolator());
        }
        if (animatorDrawingAppearing != null) {
            animatorDrawingAppearing.cancel();
        }
        stateTextButton.setState(StateTextButton.STATE_DEFAULT);
        buttonAddDrawing.setState(TwoStateImageButton.STATE_DEFAULT);
        buttonAddEmoji.setState(TwoStateImageButton.STATE_DEFAULT);
        animatorDrawingDisappearing.start();

        UiHelper.gone(colorPicker);

        if (buttonDone != null) {
            buttonDone.show();
        }
    }

    @Override
    public void onPathStart() {
        hideControlsAfterDrawingHasStarted();
    }

    @Override
    public void onNewPathDrawn() {
        showControlsAfterDrawingHasFinished();
    }

    @Override
    public void onBackPressed() {
        if (previewRootLayout == null || drawView == null) {
            super.onBackPressed();
        } else if (drawView.isEnabled()) {
            finishDrawing();
        } else if (!previewRootLayout.hideEmojiesList()) {
            super.onBackPressed();
        }
    }
}
