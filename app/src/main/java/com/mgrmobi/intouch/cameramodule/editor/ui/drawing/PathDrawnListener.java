package com.mgrmobi.intouch.cameramodule.editor.ui.drawing;

/**
 * Created by Riccardo on 22/11/16.
 */

public interface PathDrawnListener {

    void onPathStart();

    void onNewPathDrawn();
}
