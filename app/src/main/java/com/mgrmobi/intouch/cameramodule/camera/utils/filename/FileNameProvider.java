package com.mgrmobi.intouch.cameramodule.camera.utils.filename;

import android.support.annotation.Nullable;

import java.io.File;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */

public interface FileNameProvider {

    @Nullable
    File getVideoFile();

    @Nullable
    String getVideoFilePath();

    @Nullable
    File getImageFile();

    @Nullable
    String getImageFilePath();
}
