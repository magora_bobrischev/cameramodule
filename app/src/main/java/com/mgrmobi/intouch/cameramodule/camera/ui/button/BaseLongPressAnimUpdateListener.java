package com.mgrmobi.intouch.cameramodule.camera.ui.button;

import android.animation.ValueAnimator;
import android.graphics.RectF;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public abstract class BaseLongPressAnimUpdateListener implements ValueAnimator.AnimatorUpdateListener {
    protected final float progressWidthAfterPress;
    protected final float progressWidthAdd;
    protected final float progressRadiusAdd;
    protected final RectF progressRectAfterPress;

    public BaseLongPressAnimUpdateListener(
            float progressWidthAfterPress,
            float progressWidthAfterLongPress,
            float progressRadiusAfterPress,
            float progressRadiusAfterLongPress,
            RectF originalProgressBackgroundRect) {
        this.progressWidthAfterPress = progressWidthAfterPress;

        this.progressWidthAdd = progressWidthAfterLongPress - progressWidthAfterPress;

        this.progressRadiusAdd = progressRadiusAfterLongPress - progressRadiusAfterPress;

        float radiusDiff = progressRadiusAfterPress - originalProgressBackgroundRect.width() / 2;
        this.progressRectAfterPress = new RectF(
                originalProgressBackgroundRect.left - radiusDiff,
                originalProgressBackgroundRect.top - radiusDiff,
                originalProgressBackgroundRect.right + radiusDiff,
                originalProgressBackgroundRect.bottom + radiusDiff
        );
    }
}