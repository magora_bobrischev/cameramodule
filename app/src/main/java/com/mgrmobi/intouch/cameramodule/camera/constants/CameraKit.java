package com.mgrmobi.intouch.cameramodule.camera.constants;

import android.content.res.Resources;

public class CameraKit {

    public static class Internal {

        public static final int SCREEN_WIDTH = Resources.getSystem().getDisplayMetrics().widthPixels;
        public static final int SCREEN_HEIGHT = Resources.getSystem().getDisplayMetrics().heightPixels;

    }

    public static class Constants {

        public static final int PERMISSION_REQUEST_CAMERA = 16;

        public static final int FACING_BACK = 0;
        public static final int FACING_FRONT = 1;

        public static final int FLASH_OFF = 0;
        public static final int FLASH_ON = 1;
        public static final int FLASH_AUTO = 2;

        public static final int METHOD_STANDARD = 0;
        public static final int METHOD_STILL = 1;
        public static final int METHOD_SPEED = 2;

        public static final int FOCUS_OFF = 0;
        public static final int FOCUS_CONTINUOUS = 1;
        public static final int FOCUS_TAP = 2;

        public static final int ZOOM_OFF = 0;
        public static final int ZOOM_PINCH = 1;

    }

    public static class Defaults {

        public static final int DEFAULT_FACING = Constants.FACING_BACK;
        public static final int DEFAULT_FLASH = Constants.FLASH_AUTO;
        public static final int DEFAULT_FOCUS = Constants.FOCUS_OFF;
        public static final int DEFAULT_METHOD = Constants.METHOD_STANDARD;
        public static final int DEFAULT_ZOOM = Constants.ZOOM_OFF;

        public static final int DEFAULT_JPEG_QUALITY = 90;
        public static final boolean DEFAULT_CROP_OUTPUT = false;
        public static final boolean DEFAULT_ADJUST_VIEW_BOUNDS = false;

    }

}
