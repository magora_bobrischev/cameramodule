package com.mgrmobi.intouch.cameramodule.camera.camera;

import com.mgrmobi.intouch.cameramodule.camera.types.*;
import com.mgrmobi.intouch.cameramodule.camera.utils.Size;

abstract class CameraImpl {

    protected final CameraListener mCameraListener;
    protected final PreviewImpl mPreview;

    CameraImpl(CameraListener callback, PreviewImpl preview) {
        mCameraListener = callback;
        mPreview = preview;
    }

    abstract void start();

    abstract void stop();

    abstract void setDisplayOrientation(int displayOrientation);

    abstract void setFacing(@Facing int facing);

    abstract void setFlash(@Flash int flash);

    abstract void setFocus(@Focus int focus);

    abstract void setMethod(@Method int method);

    abstract void setZoom(@Zoom int zoom);

    public void setPreferredCaptureSize(int width, int height) {
    }

    public void autoResumeAfterPhotoWasTaken(boolean autoStart) {

    }

    public void startPreview() {

    }

    public void stopPreview() {

    }

    abstract void captureImage();

    abstract void startVideo();

    abstract void endVideo();

    abstract Size getCaptureResolution();

    abstract Size getPreviewResolution();

    abstract boolean isCameraOpened();

    abstract int getCameraImageRotation();

}
