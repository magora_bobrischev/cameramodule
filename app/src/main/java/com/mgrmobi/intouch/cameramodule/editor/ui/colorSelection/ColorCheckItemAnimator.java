package com.mgrmobi.intouch.cameramodule.editor.ui.colorSelection;

import android.animation.*;
import android.support.annotation.NonNull;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */
public class ColorCheckItemAnimator extends DefaultItemAnimator {
    private static final long DURATION_CHECKED_ANIM_MILLIS = 200;
    private static final long DURATION_UNCHECKED_ANIM_MILLIS = 180;
    private static final TimeInterpolator INTERPOLATOR_CHECKED = new OvershootInterpolator(3.8f);
    private static final TimeInterpolator INTERPOLATOR_CHECKED_BORDER = new DecelerateInterpolator(5);
    private static final TimeInterpolator INTERPOLATOR_UNCHECKED = new FastOutSlowInInterpolator();
    private final Map<RecyclerView.ViewHolder, AnimatorSet> colorAnimationsMap = new HashMap<>();

    @Override
    public boolean canReuseUpdatedViewHolder(@NonNull RecyclerView.ViewHolder viewHolder) {
        return true;
    }

    @NonNull
    @Override
    public ItemHolderInfo recordPreLayoutInformation(@NonNull RecyclerView.State state,
                                                     @NonNull RecyclerView.ViewHolder viewHolder,
                                                     int changeFlags, @NonNull List<Object> payloads) {
        if (changeFlags == FLAG_CHANGED) {
            for (Object payload : payloads) {
                if (payload instanceof Boolean) {
                    return new ColorItemHolderInfo((boolean) payload);
                }
            }
        }

        return super.recordPreLayoutInformation(state, viewHolder, changeFlags, payloads);
    }

    @Override
    public boolean animateChange(@NonNull RecyclerView.ViewHolder oldHolder,
                                 @NonNull RecyclerView.ViewHolder newHolder,
                                 @NonNull ItemHolderInfo preInfo,
                                 @NonNull ItemHolderInfo postInfo) {
        cancelCurrentAnimationIfExists(newHolder);

        if (preInfo instanceof ColorItemHolderInfo) {
            ColorItemHolderInfo colorItemHolderInfo = (ColorItemHolderInfo) preInfo;
            HolderColorItem holder = (HolderColorItem) newHolder;

            if (colorItemHolderInfo.updateAction) {
                animateColorSelected(holder);
            } else {
                animateColorUnselected(holder);
            }
        }

        return false;
    }

    private void cancelCurrentAnimationIfExists(RecyclerView.ViewHolder item) {
        if (colorAnimationsMap.containsKey(item)) {
            colorAnimationsMap.get(item).cancel();
        }
    }

    private void animateColorSelected(HolderColorItem holder) {
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator border = ObjectAnimator.ofInt(holder.colorImage, "borderWidth", holder.colorImage.getBorderWidth(), 0);
        border.setInterpolator(INTERPOLATOR_CHECKED_BORDER);
        ObjectAnimator content = ObjectAnimator.ofInt(holder.colorImage, "currentRadius", holder.colorImage.getCurrentRadius(), holder.colorImage.getMaximumRadius());
        content.setInterpolator(INTERPOLATOR_CHECKED);
        set.playTogether(border, content);
        set.setDuration(DURATION_CHECKED_ANIM_MILLIS);

        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                colorAnimationsMap.remove(holder);
                dispatchChangeFinishedIfAllAnimationsEnded(holder);
            }
        });
        set.start();

        colorAnimationsMap.put(holder, set);
    }

    private void dispatchChangeFinishedIfAllAnimationsEnded(HolderColorItem holder) {
        if (colorAnimationsMap.containsKey(holder)) {
            return;
        }

        dispatchAnimationFinished(holder);
    }


    private void animateColorUnselected(HolderColorItem holder) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofInt(holder.colorImage, "borderWidth", holder.colorImage.getBorderWidth(), holder.colorImage.getInitialBorderWidth()),
                ObjectAnimator.ofInt(holder.colorImage, "currentRadius", holder.colorImage.getCurrentRadius(), holder.colorImage.getInitialRadius())

        );
        set.setInterpolator(INTERPOLATOR_UNCHECKED);
        set.setDuration(DURATION_UNCHECKED_ANIM_MILLIS);

        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                colorAnimationsMap.remove(holder);
                dispatchChangeFinishedIfAllAnimationsEnded(holder);
            }
        });
        set.start();

        colorAnimationsMap.put(holder, set);
    }

    private static class ColorItemHolderInfo extends ItemHolderInfo {
        boolean updateAction;

        ColorItemHolderInfo(boolean updateAction) {
            this.updateAction = updateAction;
        }
    }
}
