package com.mgrmobi.intouch.cameramodule.editor.utils;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public class UiHelper {

    private UiHelper() {
    }

    public static void visible(@NonNull View... views) {
        for (View v : views) {
            if (v != null) {
                v.setVisibility(View.VISIBLE);
            }
        }
    }

    public static void invisible(@NonNull View... views) {
        for (View v : views) {
            if (v != null) {
                v.setVisibility(View.INVISIBLE);
            }
        }
    }

    public static void gone(@NonNull View... views) {
        for (View v : views) {
            if (v != null) {
                v.setVisibility(View.GONE);
            }
        }
    }

    public static void enable(@NonNull View... views) {
        for (View v : views) {
            if (v != null) {
                v.setEnabled(true);
            }
        }
    }

    public static void disable(@NonNull View... views) {
        for (View v : views) {
            if (v != null) {
                v.setEnabled(false);
            }
        }
    }

    public static void waitForMeasuring(@Nullable View view, @NonNull Runnable callback) {
        if (view != null) {
            if (view.getMeasuredWidth() != 0 && view.getMeasuredHeight() != 0) {
                callback.run();
            } else {
                view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    private boolean called;

                    @Override
                    public void onGlobalLayout() {
                        view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        if (!called) {
                            callback.run();
                            called = true;
                        }
                    }
                });
            }
        }
    }

    public static void elevate(@Nullable View v, float elevation) {
        if (v != null) {
            ViewCompat.setElevation(v, elevation);
        }
    }

    public static void elevateDp(@Nullable View v, float elevationDp) {
        if (v != null) {
            ViewCompat.setElevation(v, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, elevationDp, Resources.getSystem().getDisplayMetrics()));
        }
    }

    public static int dp2px_i(int dp) {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, Resources.getSystem().getDisplayMetrics()));
    }
}
