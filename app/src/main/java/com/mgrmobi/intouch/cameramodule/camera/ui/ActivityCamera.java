package com.mgrmobi.intouch.cameramodule.camera.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import intouch.mgrmobi.com.intouchcameramodule.R;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public class ActivityCamera extends AppCompatActivity implements FragmentCamera.CameraFragmentParent {

    public static Intent createIntent(Context callingContext) {
        return new Intent(callingContext, ActivityCamera.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, FragmentCamera.makeFragment(), FragmentCamera.class.getName()).commit();
    }

    @Override
    public void onCancelCamera() {
        onBackPressed();
    }
}
