package com.mgrmobi.intouch.cameramodule.editor.viewmodel;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public class ImageLayer extends Layer {

    public ImageLayer() {
    }

    @Override
    protected void reset() {
        super.reset();
    }

    @Override
    protected float getMaxScale() {
        return Limits.MAX_SCALE;
    }

    @Override
    protected float getMinScale() {
        return Limits.MIN_SCALE;
    }

    @Override
    public float initialScale() {
        return Limits.INITIAL_SCALE;
    }

    public interface Limits {
        /**
         * limit text size to view bounds
         * so that users don't put small font size and scale it 100+ times
         */
        float MAX_SCALE = 10F;
        float MIN_SCALE = 0.5F;
        float MIN_BITMAP_HEIGHT = 0.13F;
        float INITIAL_SCALE = 1.5F;
    }
}
