package com.mgrmobi.intouch.cameramodule.camera.types;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.mgrmobi.intouch.cameramodule.camera.constants.CameraKit.Constants.METHOD_STANDARD;
import static com.mgrmobi.intouch.cameramodule.camera.constants.CameraKit.Constants.METHOD_STILL;


@Retention(RetentionPolicy.SOURCE)
@IntDef({METHOD_STANDARD, METHOD_STILL})
public @interface Method {
}
