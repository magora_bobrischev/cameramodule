package com.mgrmobi.intouch.cameramodule.camera.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import intouch.mgrmobi.com.intouchcameramodule.BuildConfig;
import intouch.mgrmobi.com.intouchcameramodule.R;
import com.mgrmobi.intouch.cameramodule.camera.camera.CameraListener;
import com.mgrmobi.intouch.cameramodule.camera.camera.CameraView;
import com.mgrmobi.intouch.cameramodule.camera.constants.CameraKit;
import com.mgrmobi.intouch.cameramodule.camera.ui.button.CameraRecordControlButton;
import com.mgrmobi.intouch.cameramodule.camera.utils.filename.DefaultFileNameProvider;
import com.mgrmobi.intouch.cameramodule.camera.utils.filename.FileNameProvider;

import java.io.File;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public class FragmentCamera extends Fragment implements View.OnLayoutChangeListener {
    @Bind(R.id.camera_view) CameraView cameraView;
    @Bind(R.id.focus_marker) FocusMarkerLayout focusMarker;
    @Bind(R.id.camera_control_button) CameraRecordControlButton cameraControlButton;
    @Bind(R.id.button_flash) ImageView buttonFlash;
    private FileNameProvider fileNameProvider = new DefaultFileNameProvider();

    public interface CameraFragmentParent {

        void onCancelCamera();
    }

    public static FragmentCamera makeFragment() {
        return new FragmentCamera();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_camera, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        cameraControlButton.setControlListener(new CameraRecordControlButton.ControllerActionListener() {

            @Override
            public void onFirstTouch() {

            }

            @Override
            public void onFinalRelease() {

            }

            @Override
            public void onSinglePress(@NonNull CameraRecordControlButton button) {
                if (cameraView != null) {
                    cameraView.captureImage();
                }
            }

            @Override
            public void onLongPress(@NonNull CameraRecordControlButton button) {

            }

            @Override
            public void onLongPressPreRelease(@NonNull CameraRecordControlButton button) {

            }

            @Override
            public void onLongPressRelease(@NonNull CameraRecordControlButton button) {

            }

            @Override
            public void onCancel(@NonNull CameraRecordControlButton button) {

            }
        });

        cameraView.addOnLayoutChangeListener(this);

        cameraView.setCameraListener(new CameraListener() {

            @Override
            public void onCameraOpened() {

            }

            @Override
            public void onCameraClosed() {

            }

            @Override
            public void onPictureTaken(byte[] jpeg) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length);
                ResultHolder.dispose();
                ResultHolder.setImage(bitmap);
                startActivity(ActivityPickedPhotoPreview.createIntent(getActivity()));
            }

            @Override
            public void onVideoTaken(File video) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (cameraView != null) {
            cameraView.start();
        }
    }

    @Override
    public void onPause() {
        if (cameraView != null) {
            cameraView.stop();
        }
        super.onPause();
    }

    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        if (cameraView != null) {
            cameraView.removeOnLayoutChangeListener(this);
        }
    }

    @OnClick(R.id.button_flash)
    void toggleFlash() {
        if (cameraView == null) {
            return;
        }
        int iconResId;
        switch (cameraView.toggleFlash()) {
            case CameraKit.Constants.FLASH_ON:
                iconResId = R.drawable.ic_flash_on;
                break;
            case CameraKit.Constants.FLASH_AUTO:
                iconResId = R.drawable.ic_flash_auto;
                break;
            case CameraKit.Constants.FLASH_OFF:
            default:
                iconResId = R.drawable.ic_flash_off;
                break;
        }
        if (buttonFlash != null) {
            buttonFlash.setImageResource(iconResId);
        }
    }

    @OnClick(R.id.button_switch_cameras)
    void toggleCamera() {
        if (cameraView == null) {
            return;
        }
        switch (cameraView.toggleFacing()) {
            case CameraKit.Constants.FACING_BACK:
                if (BuildConfig.DEBUG) {
                    Toast.makeText(getContext(), "DEBUG MESSAGE: Need BACK facing icon!!!111", Toast.LENGTH_SHORT).show();
                }
                break;

            case CameraKit.Constants.FACING_FRONT:
                if (BuildConfig.DEBUG) {
                    Toast.makeText(getContext(), "DEBUG MESSAGE: Need FRONT facing icon!!!111", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @OnTouch(R.id.focus_marker)
    boolean onTouchCamera(MotionEvent motionEvent) {
        if (focusMarker != null) {
            focusMarker.focus(motionEvent.getX(), motionEvent.getY());
        }
        return false;
    }

    @OnClick(R.id.button_cancel_camera)
    void onCancelButtonClicked() {
        if (getActivity() instanceof CameraFragmentParent) {
            ((CameraFragmentParent) getActivity()).onCancelCamera();
        }
    }
}
