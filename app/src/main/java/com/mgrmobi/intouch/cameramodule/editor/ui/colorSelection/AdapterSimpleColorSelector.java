package com.mgrmobi.intouch.cameramodule.editor.ui.colorSelection;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import com.mgrmobi.intouch.cameramodule.editor.Config;
import intouch.mgrmobi.com.intouchcameramodule.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */
public class AdapterSimpleColorSelector extends RecyclerView.Adapter<HolderColorItem> {
    private static final int ITEMS_TO_ANIMATE = Config.COLORS_ON_SCREEN;
    private static final Interpolator INTERPOLATOR = new OvershootInterpolator(5);
    private final List<ColorData> colors;
    private final ItemSelectionCallback callback;
    private int lastPosition = -1;


    public interface ItemSelectionCallback {
        void onItemSelected(@NonNull ColorData item);
    }

    public AdapterSimpleColorSelector(int[] colors, @NonNull ItemSelectionCallback callback) {
        if (colors.length == 0) {
            throw new IllegalArgumentException("Colors must not be empty");
        }
        this.colors = new ArrayList<>(colors.length);
        this.callback = callback;
        for (int color : colors) {
            this.colors.add(ColorData.of(color));
        }
    }

    @Override
    public HolderColorItem onCreateViewHolder(ViewGroup parent, int viewType) {
        return HolderColorItem.createHolder(parent, item -> {
            flipCheckedItems(item);
            callback.onItemSelected(item);
        });
    }

    private void flipCheckedItems(@NonNull ColorData dataToSelect) {
        boolean wasUnchecked = false;
        boolean wasChecked = false;
        for (int i = 0, size = colors.size(); i < size; ++i) {
            ColorData d = colors.get(i);
            if (d.isChecked) {
                if (d.color == dataToSelect.color) {
                    return;
                }
                d.isChecked = false;
                notifyItemChanged(i, false);
                wasUnchecked = true;
            } else if (d.color == dataToSelect.color) {
                d.isChecked = true;
                notifyItemChanged(i, true);
                wasChecked = true;
            }
            if (wasChecked && wasUnchecked) {
                return;
            }
        }
    }

    @Override
    public void onBindViewHolder(HolderColorItem holder, int position) {
        holder.bind(colors.get(position));
        setAnimation(holder.colorImage, position);
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position < ITEMS_TO_ANIMATE && position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), R.anim.color_item_appearing);
            animation.setStartOffset(position * 20 + 60);
            animation.setInterpolator(INTERPOLATOR);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        return colors.size();
    }

    public void selectColor(@Nullable ColorData color) {
        if (color == null) {
            color = colors.get(0);
        }
        flipCheckedItems(color);
        callback.onItemSelected(color);
    }
}
