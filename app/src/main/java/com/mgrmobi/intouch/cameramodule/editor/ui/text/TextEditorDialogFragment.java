package com.mgrmobi.intouch.cameramodule.editor.ui.text;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Selection;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.mgrmobi.intouch.cameramodule.editor.ui.colorSelection.WidgetSimpleColorSelector;
import com.mgrmobi.intouch.cameramodule.editor.utils.UiHelper;
import intouch.mgrmobi.com.intouchcameramodule.R;

/**
 * Transparent Dialog Fragment, with no title and no background
 * <p>
 * The fragment imitates capturing input from keyboard, but does not display anything
 * the result from input from the keyboard is passed through {@link TextEditorDialogFragment.OnTextLayerCallback}
 * <p>
 * Activity that uses {@link TextEditorDialogFragment} must implement {@link TextEditorDialogFragment.OnTextLayerCallback}
 * <p>
 * If Activity does not implement {@link TextEditorDialogFragment.OnTextLayerCallback}, exception will be thrown at Runtime
 */
public class TextEditorDialogFragment extends DialogFragment implements WidgetSimpleColorSelector.ColorSelectionListener {
    private static final int NO_COLOR = Integer.MIN_VALUE;
    private static final String ARG_TEXT = "TextEditorDialogFragment.ARG_TEXT";
    private static final String ARG_TEXT_COLOR = "TextEditorDialogFragment.ARG_TEXT_COLOR";
    @Bind(R.id.edit_text_view) EditText editText;
    @Bind(R.id.widget_color_picker) WidgetSimpleColorSelector colorSelector;
    private OnTextLayerCallback callback;

    public interface OnTextLayerCallback {

        void onTextEntered(@NonNull String text, @ColorInt int selectedColor);

        void onDismissed();
    }

    public static TextEditorDialogFragment getInstance() {
        TextEditorDialogFragment fragment = new TextEditorDialogFragment();
        fragment.setArguments(new Bundle());

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.text_editor_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        colorSelector.setColorSelectionListener(this);

        // exit when clicking on background
        view.findViewById(R.id.text_editor_root).setOnClickListener(view1 -> {
            textEntered();
            dismiss();
        });
    }

    public void setTextAndColor(@Nullable String text, @ColorInt int color) {
        getArguments().putString(ARG_TEXT, text);
        if (text != null) {
            getArguments().putInt(ARG_TEXT_COLOR, color);
        } else {
            getArguments().remove(ARG_TEXT_COLOR);
        }
    }

    @Override
    public void onColorSelected(@ColorInt int color) {
        if (editText != null) {
            editText.setTextColor(color);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (callback() != null) {
            callback().onDismissed();
        }
    }

    @Override
    public void onDetach() {
        // release links
        this.callback = null;
        super.onDetach();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null) {
                // remove background
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

                WindowManager.LayoutParams windowParams = window.getAttributes();
                window.setDimAmount(0.6F);
                window.setAttributes(windowParams);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        initColorSelectorWithInitialColor();

        initEditTextWithInitialText();
    }

    private void initColorSelectorWithInitialColor() {
        if (colorSelector != null) {
            int preferredColor = getArguments().getInt(ARG_TEXT_COLOR, NO_COLOR);
            if (preferredColor != NO_COLOR) {
                colorSelector.setInitialColor(preferredColor);
            }
        }
    }

    private void initEditTextWithInitialText() {
        editText.post(() -> {
            if (editText != null) {
                String text = getArguments().getString(ARG_TEXT);
                editText.setText(text);
                Selection.setSelection(editText.getText(), editText.length());

                listenToSoftKeyboardEvents();

                editText.setFocusableInTouchMode(true);
                editText.setFocusable(true);
                editText.requestFocus();

                // force show the keyboard
                InputMethodManager ims = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                ims.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
            }
        });
    }

    @OnClick(R.id.button_cancel)
    void onCancelButtonClicked() {
        dismiss();
    }

    @OnClick(R.id.button_accept_text)
    void onAcceptButtonClicked() {
        textEntered();
        dismiss();
    }

    private void textEntered() {
        if (callback() != null && editText != null && colorSelector != null) {
            callback().onTextEntered(editText.getText().toString(), colorSelector.getSelectedColor());
        }
    }

    private void listenToSoftKeyboardEvents() {
        View view = getView();
        if (view != null) {
            view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                private boolean keyboardVisible;

                @Override
                public void onGlobalLayout() {
                    Rect r = new Rect();
                    getView().getWindowVisibleDisplayFrame(r);
                    if (getView().getRootView().getHeight() - (r.bottom - r.top) > 100) { // if more than 100 pixels, its probably a keyboard...
                        if (!keyboardVisible) {
                            UiHelper.waitForMeasuring(colorSelector, () -> {
                                if (colorSelector != null) {
                                    ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) colorSelector.getLayoutParams();
                                    params.topMargin = r.bottom - r.top - colorSelector.getMeasuredHeight() - params.bottomMargin;
                                    colorSelector.requestLayout();
                                    colorSelector.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                        keyboardVisible = true;
                    } else {
                        if (keyboardVisible) {
                            keyboardVisible = false;
                            dismiss();
                        }
                    }
                }
            });
        }
    }

    private OnTextLayerCallback callback() {
        if (callback == null) {
            if (getContext() instanceof OnTextLayerCallback) {
                callback = (OnTextLayerCallback) getContext();
            } else {
                throw new IllegalStateException(getContext().getClass().getName() + " must implement " + OnTextLayerCallback.class.getName());
            }
        }
        return callback;
    }
}