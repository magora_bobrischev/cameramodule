package com.mgrmobi.intouch.cameramodule.editor;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public interface Config {

    int COLORS_ON_SCREEN = 7;
}
