package com.mgrmobi.intouch.cameramodule.editor.ui.drawing;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Riccardo Moro on 9/25/2016.
 */

public class Point implements Parcelable {
    float x = -1;
    float y = -1;

    public Point() {
    }

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }

    private Point(Parcel in) {
        x = in.readFloat();
        y = in.readFloat();
    }

    @Override
    public String toString() {
        return "" + x + "," + y;
    }


    // Parcelable stuff
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(x);
        dest.writeFloat(y);
    }

    // Parcelable CREATOR class
    public static final Creator<Point> CREATOR = new Creator<Point>() {
        @Override
        public Point createFromParcel(Parcel in) {
            return new Point(in);
        }

        @Override
        public Point[] newArray(int size) {
            return new Point[size];
        }
    };

}
