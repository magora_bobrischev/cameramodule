package com.mgrmobi.intouch.cameramodule.editor.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.mgrmobi.intouch.cameramodule.editor.utils.UiHelper;
import intouch.mgrmobi.com.intouchcameramodule.R;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */
public class EmojiesBlurLinearLayout extends BlurLinearLayout {
    private static final int DEFAULT_CORNERS_RADIUS = UiHelper.dp2px_i(8);
    private final float heightPercent;
    private final float topCornersRadius;
    private final Path clipPath = new Path();

    public EmojiesBlurLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.EmojiesBlurLinearLayout, 0, 0);

        try {
            heightPercent = array.getFloat(R.styleable.EmojiesBlurLinearLayout_ebfl_heightPercent, 0.6f);
            topCornersRadius = array.getDimension(R.styleable.EmojiesBlurLinearLayout_ebfl_topCornersRadius, DEFAULT_CORNERS_RADIUS);
        } finally {
            array.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int h = Math.round(MeasureSpec.getSize(heightMeasureSpec) * heightPercent);
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(h, MeasureSpec.EXACTLY));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        int vOffsetPx = 0;
        float rectSize = topCornersRadius * 2;
        clipPath.reset();
        clipPath.moveTo(0, h);
        clipPath.lineTo(0, vOffsetPx + rectSize);
        RectF rect = new RectF(0, vOffsetPx, rectSize, vOffsetPx + rectSize);
        clipPath.arcTo(rect, 180, 90, false);
        clipPath.lineTo(w - rectSize, vOffsetPx);
        rect.set(w - rectSize, vOffsetPx, w, vOffsetPx + rectSize);
        clipPath.arcTo(rect, 270, 90, false);
        clipPath.lineTo(w, h);
        clipPath.close();
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        canvas.clipPath(clipPath);
        super.draw(canvas);
        canvas.restore();
    }
}
