package com.mgrmobi.intouch.cameramodule.camera.utils;

import android.graphics.*;
import com.facebook.imagepipeline.nativecode.JpegTranscoder;
import com.jni.bitmap_operations.JniBitmapHolder;
import intouch.mgrmobi.com.intouchcameramodule.BuildConfig;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class CenterCrop {
    private byte[] croppedJpeg;

    public CenterCrop(YuvImage yuv, AspectRatio targetRatio, int jpegCompression) {
        Rect crop = getCrop(yuv.getWidth(), yuv.getHeight(), targetRatio);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        yuv.compressToJpeg(crop, jpegCompression, out);
        this.croppedJpeg = out.toByteArray();
    }

    public CenterCrop(byte[] jpeg, AspectRatio targetRatio, int jpegCompression) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length, options);

        Rect crop = getCrop(options.outWidth, options.outHeight, targetRatio);
        try {
            Bitmap bitmap = BitmapRegionDecoder.newInstance(jpeg, 0, jpeg.length, true).decodeRegion(crop, null);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, jpegCompression, out);
            this.croppedJpeg = out.toByteArray();
        } catch (IOException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public CenterCrop(byte[] jpeg, AspectRatio targetRatio, int jpegCompression, int requiredRotation, boolean flipHorizontal) {
        if (requiredRotation == 0 && !flipHorizontal) {
            initDefault(jpeg, targetRatio, jpegCompression);
        } else if (requiredRotation != 0 && flipHorizontal) {
            initWithRotationAndFlipHorizontally(jpeg, targetRatio, jpegCompression, requiredRotation);
        } else if (requiredRotation != 0) {
            initWithRotationOnly(jpeg, targetRatio, jpegCompression, requiredRotation);
        } else {
            initFlipHorizontallyOnly(jpeg, targetRatio, jpegCompression);
        }
    }

    private void initDefault(byte[] jpeg, AspectRatio targetRatio, int jpegCompression) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length, options);

        Rect crop = getCrop(options.outWidth, options.outHeight, targetRatio);
        try {
            Bitmap bitmap = BitmapRegionDecoder.newInstance(jpeg, 0, jpeg.length, true).decodeRegion(crop, null);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, jpegCompression, out);
            this.croppedJpeg = out.toByteArray();
        } catch (IOException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    private void initWithRotationAndFlipHorizontally(byte[] jpeg, AspectRatio targetRatio, int jpegCompression, int requiredRotation) {
        InputStream in = new ByteArrayInputStream(jpeg);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            JpegTranscoder.transcodeJpeg(in, out, requiredRotation, 8, 100);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            jpeg = out.toByteArray();
            BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length, options);

            Rect crop = getCrop(options.outWidth, options.outHeight, targetRatio);
            Bitmap bitmap = BitmapRegionDecoder.newInstance(jpeg, 0, jpeg.length, true).decodeRegion(crop, null);
            JniBitmapHolder holder = new JniBitmapHolder();
            holder.storeBitmap(bitmap);
            holder.flipBitmapHorizontal();
            bitmap = holder.getBitmapAndFree();
            out = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, jpegCompression, out);
            this.croppedJpeg = out.toByteArray();
        } catch (IOException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    private void initWithRotationOnly(byte[] jpeg, AspectRatio targetRatio, int jpegCompression, int requiredRotation) {
        InputStream in = new ByteArrayInputStream(jpeg);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            JpegTranscoder.transcodeJpeg(in, out, requiredRotation, 8, 100);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            jpeg = out.toByteArray();
            BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length, options);

            Rect crop = getCrop(options.outWidth, options.outHeight, targetRatio);
            Bitmap bitmap = BitmapRegionDecoder.newInstance(jpeg, 0, jpeg.length, true).decodeRegion(crop, null);
            out = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, jpegCompression, out);
            this.croppedJpeg = out.toByteArray();
        } catch (IOException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    private void initFlipHorizontallyOnly(byte[] jpeg, AspectRatio targetRatio, int jpegCompression) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length, options);

        Rect crop = getCrop(options.outWidth, options.outHeight, targetRatio);
        try {
            Bitmap bitmap = BitmapRegionDecoder.newInstance(jpeg, 0, jpeg.length, true).decodeRegion(crop, null);
            JniBitmapHolder holder = new JniBitmapHolder();
            holder.storeBitmap(bitmap);
            holder.flipBitmapHorizontal();
            bitmap = holder.getBitmapAndFree();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, jpegCompression, out);
            this.croppedJpeg = out.toByteArray();
        } catch (IOException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    private static Rect getCrop(int currentWidth, int currentHeight, AspectRatio targetRatio) {
        AspectRatio currentRatio = AspectRatio.of(currentWidth, currentHeight);

        Rect crop;
        if (currentRatio.toFloat() > targetRatio.toFloat()) {
            int width = (int) (currentHeight * targetRatio.toFloat());
            int widthOffset = (currentWidth - width) / 2;
            crop = new Rect(widthOffset, 0, currentWidth - widthOffset, currentHeight);
        } else {
            int height = (int) (currentWidth * targetRatio.inverse().toFloat());
            int heightOffset = (currentHeight - height) / 2;
            crop = new Rect(0, heightOffset, currentWidth, currentHeight - heightOffset);
        }

        return crop;
    }

    public byte[] getJpeg() {
        return croppedJpeg;
    }

}
