package com.mgrmobi.intouch.cameramodule.camera.ui.button;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.*;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.HapticFeedbackConstants;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import intouch.mgrmobi.com.intouchcameramodule.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class CameraRecordControlButton extends View {
    private static final long ANIMATION_COLLAPSE_DURATION_MILLIS = 150;
    private static final float PROGRESS_START_POSITION_ANGLE = -90;
    private static final float PROGRESS_MAX_ANGLE = 360;
    private static final int STATE_NOT_PRESSED = 0;
    private static final int STATE_PRESSED = 1;
    private static final int STATE_LONG_PRESSED = 2;

    private static final TimeInterpolator PRESS_EXPAND_INTERPOLATOR = new AccelerateInterpolator(2f);
    private static final TimeInterpolator LONG_PRESS_EXPAND_INTERPOLATOR = new AccelerateInterpolator(3f);
    private static final TimeInterpolator PRESS_COLLAPSE_INTERPOLATOR = new AccelerateInterpolator(2f);
    private static final TimeInterpolator LONG_PRESS_COLLAPSE_INTERPOLATOR = new AccelerateInterpolator(2f);

    private final Paint paintProgressBackground;
    private final Paint paintProgressIndicator;
    private final Paint paintButton;

    private final RectF rectProgressBackground;
    private final RectF rectProgress;
    private final RectF rectButton;

    private float startProgressRadius;
    private float startProgressWidth;
    private float progressWidthAfterPress;
    private float progressWidthAfterLongPress;
    private float progressIndicatorWidth;
    private float gapBetweenProgressAndButton;
    private float buttonRadiusAfterPress;
    private float buttonRadiusAfterLongPress;
    private float progressRadiusAfterPress;
    private float progressRadiusAfterLongPress;

    private int singlePressDuration;
    private int longPressDuration;
    private int singlePressThreshold;
    private long pointerDownTime;

    private int buttonRadius;

    @ColorInt private int progressDefaultColor;
    @ColorInt private int progressPressedColor;
    @ColorInt private int progressLongPressedColor;

    @ColorInt private int progressIndicatorColor;

    @ColorInt private int buttonDefaultColor;
    @ColorInt private int buttonPressedColor;
    @ColorInt private int buttonLongPressedColor;

    @ColorInt private int currentButtonColor;

    private float centerX;
    private float centerY;

    @FloatRange(from = 0, to = 100) private float currentProgressPercent;
    private float currentProgressAngles;

    private boolean isCancelled;
    private boolean updateProgressIndicatorAlphaWhenUpdateProgress;
    private boolean longTapEnabled = true;

    private ControllerActionListener controllerActionListener;

    private LongPressExpandListener longPressExpandListener;
    private PressExpandListener pressExpandListener;

    private ValueAnimator longPressAnimator;
    private ValueAnimator pressAnimator;

    @IntDef({STATE_NOT_PRESSED, STATE_PRESSED, STATE_LONG_PRESSED})
    @Retention(RetentionPolicy.SOURCE)
    private @interface CurrentState {

    }

    @CurrentState private int currentState;

    public interface ControllerActionListener {

        void onFirstTouch();

        void onFinalRelease();

        void onSinglePress(@NonNull CameraRecordControlButton button);

        void onLongPress(@NonNull CameraRecordControlButton button);

        void onLongPressPreRelease(@NonNull CameraRecordControlButton button);

        void onLongPressRelease(@NonNull CameraRecordControlButton button);

        void onCancel(@NonNull CameraRecordControlButton button);
    }

    public CameraRecordControlButton(Context context) {
        super(context);
        rectButton = new RectF();
        rectProgressBackground = new RectF();
        rectProgress = new RectF();

        paintProgressBackground = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintProgressIndicator = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintButton = new Paint(Paint.ANTI_ALIAS_FLAG);

        init(context, null);
    }

    public CameraRecordControlButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        rectButton = new RectF();
        rectProgressBackground = new RectF();
        rectProgress = new RectF();

        paintProgressBackground = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintProgressIndicator = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintButton = new Paint(Paint.ANTI_ALIAS_FLAG);

        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {

        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CameraControllerRecordButton, 0, 0);

        startProgressWidth = array.getDimension(R.styleable.CameraControllerRecordButton_ccrb_startProgressWidth, dp2px_f(4));
        progressWidthAfterPress = array.getDimension(R.styleable.CameraControllerRecordButton_ccrb_progressWidthAfterPress, startProgressWidth + dp2px_f(2));
        progressWidthAfterLongPress = array.getDimension(R.styleable.CameraControllerRecordButton_ccrb_progressWidthAfterLongPress, progressWidthAfterPress + dp2px_f(2));

        gapBetweenProgressAndButton = array.getDimension(R.styleable.CameraControllerRecordButton_ccrb_gapBetweenProgressAndButton, dp2px_f(3));

        buttonRadiusAfterPress = array.getDimension(R.styleable.CameraControllerRecordButton_ccrb_buttonRadiusAfterPress, dp2px_f(3));
        buttonRadiusAfterLongPress = array.getDimension(R.styleable.CameraControllerRecordButton_ccrb_buttonRadiusAfterLongPress, dp2px_f(3));

        startProgressRadius = array.getDimension(R.styleable.CameraControllerRecordButton_ccrb_startProgressRadius, dp2px_f(64));
        progressRadiusAfterPress = array.getDimension(R.styleable.CameraControllerRecordButton_ccrb_progressRadiusAfterPress, startProgressRadius + dp2px_f(4));
        progressRadiusAfterLongPress = array.getDimension(R.styleable.CameraControllerRecordButton_ccrb_progressRadiusAfterLongPress, progressRadiusAfterPress + dp2px_f(4));
        progressIndicatorWidth = array.getDimension(R.styleable.CameraControllerRecordButton_ccrb_progressIndicatorWidth, progressWidthAfterLongPress);

        singlePressDuration = array.getInt(R.styleable.CameraControllerRecordButton_ccrb_singlePressDurationMillis, 500);
        longPressDuration = array.getInt(R.styleable.CameraControllerRecordButton_ccrb_longPressDurationMillis, 150);
        singlePressThreshold = array.getInt(R.styleable.CameraControllerRecordButton_ccrb_singlePressMinThresholdMillis, 150);

        progressDefaultColor = array.getColor(R.styleable.CameraControllerRecordButton_ccrb_progressDefaultColor, Color.WHITE);
        progressPressedColor = array.getColor(R.styleable.CameraControllerRecordButton_ccrb_progressPressedColor, Color.WHITE);
        progressLongPressedColor = array.getColor(R.styleable.CameraControllerRecordButton_ccrb_progressLongPressedColor, Color.WHITE);

        progressIndicatorColor = array.getColor(R.styleable.CameraControllerRecordButton_ccrb_progressIndicatorColor, Color.RED);

        buttonDefaultColor = array.getColor(R.styleable.CameraControllerRecordButton_ccrb_buttonDefaultColor, Color.WHITE);
        buttonPressedColor = array.getColor(R.styleable.CameraControllerRecordButton_ccrb_buttonPressedColor, Color.WHITE);
        buttonLongPressedColor = array.getColor(R.styleable.CameraControllerRecordButton_ccrb_buttonLongPressedColor, Color.WHITE);

        updateProgressIndicatorAlphaWhenUpdateProgress = array.getBoolean(R.styleable.CameraControllerRecordButton_ccrb_progressIndicatorUpdatesAlpha, false);

        boolean shadowUnderProgress = array.getBoolean(R.styleable.CameraControllerRecordButton_ccrb_shadowUnderProgress, false);
        if (shadowUnderProgress) {
            paintProgressBackground.setShadowLayer(5, 1, 2, Color.LTGRAY);
        }
        boolean shadowUnderProgressIndicator = array.getBoolean(R.styleable.CameraControllerRecordButton_ccrb_shadowUnderProgressIndicator, false);
        if (shadowUnderProgressIndicator) {
            paintProgressIndicator.setShadowLayer(1, 1, 2, Color.DKGRAY);
        }
        boolean shadowUnderButton = array.getBoolean(R.styleable.CameraControllerRecordButton_ccrb_shadowUnderButton, false);
        if (shadowUnderButton) {
            paintButton.setShadowLayer(2, 1, 2, Color.LTGRAY);
        }
        if (shadowUnderProgress || shadowUnderProgressIndicator || shadowUnderButton) {
            setLayerType(LAYER_TYPE_SOFTWARE, null);
        }
        setWillNotDraw(false);
        array.recycle();

        paintProgressBackground.setStyle(Paint.Style.STROKE);
        paintProgressBackground.setStrokeWidth(startProgressWidth);

        paintProgressIndicator.setStyle(Paint.Style.STROKE);
        paintProgressIndicator.setStrokeCap(Paint.Cap.ROUND);
        paintProgressIndicator.setStrokeJoin(Paint.Join.ROUND);
        paintProgressIndicator.setStrokeWidth(progressIndicatorWidth);
        paintProgressIndicator.setColor(progressIndicatorColor);

        currentButtonColor = buttonDefaultColor;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //10 - small gap to allow us use paint shadow layering
        setMeasuredDimension(
                Math.round(progressRadiusAfterLongPress * 2 + progressWidthAfterLongPress + 10 + getPaddingStart() + getPaddingEnd()),
                Math.round(progressRadiusAfterLongPress * 2 + progressWidthAfterLongPress + 10 + getPaddingTop() + getPaddingBottom())
        );
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        buttonRadius = (int) (startProgressRadius - startProgressWidth / 2 - gapBetweenProgressAndButton);

        centerX = w / 2;
        centerY = centerX;

        rectButton.set(
                centerX - buttonRadius,
                centerY - buttonRadius,
                centerX + buttonRadius,
                centerY + buttonRadius
        );

        rectProgressBackground.set(
                centerX - startProgressRadius,
                centerY - startProgressRadius,
                centerX + startProgressRadius,
                centerY + startProgressRadius
        );

        rectProgress.set(
                centerX - progressRadiusAfterLongPress,
                centerY - progressRadiusAfterLongPress,
                centerX + progressRadiusAfterLongPress,
                centerY + progressRadiusAfterLongPress
        );

        pressExpandListener = new PressExpandListener(
                startProgressWidth,
                progressWidthAfterPress,
                buttonRadiusAfterPress,
                progressRadiusAfterPress,
                new RectF(rectProgressBackground),
                new RectF(rectButton)
        );

        longPressExpandListener = new LongPressExpandListener(
                progressWidthAfterPress,
                progressWidthAfterLongPress,
                progressRadiusAfterPress,
                progressRadiusAfterLongPress,
                new RectF(rectProgressBackground)
        );
    }

    public void setCurrentProgressPercent(@FloatRange(from = 0, to = 1) float progress) {
        currentProgressPercent = progress;
        currentProgressAngles = PROGRESS_MAX_ANGLE * progress;

        if (updateProgressIndicatorAlphaWhenUpdateProgress) {
            if (progress == 0) {
                paintProgressIndicator.setAlpha(255);
            } else {
                paintProgressIndicator.setAlpha((int) ((0.5f + (0.4f * progress)) * 255));
            }
        }

        postInvalidateOnAnimation();
    }

    public float getCurrentProgressPercent() {
        return currentProgressPercent;
    }

    public void setCurrentProgressAngles(float angles) {
        currentProgressAngles = angles;
    }

    public float getCurrentProgressAngles() {
        return currentProgressAngles;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint tmpProgressPaint = paintProgressBackground;
        tmpProgressPaint.setColor(progressDefaultColor);
        RectF tmpProgress = rectProgressBackground;
        canvas.drawArc(tmpProgress, PROGRESS_START_POSITION_ANGLE, PROGRESS_MAX_ANGLE, false, tmpProgressPaint);

        float tmpProgressAngle = currentProgressAngles;
        if (tmpProgressAngle != 0f && currentState == STATE_LONG_PRESSED) {
            canvas.drawArc(rectProgress, PROGRESS_START_POSITION_ANGLE, tmpProgressAngle, false, paintProgressIndicator);
        }

        Paint tmpButtonPaint = paintButton;
        tmpButtonPaint.setColor(currentButtonColor);
        canvas.drawCircle(centerX, centerY, buttonRadius, tmpButtonPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (isEnabled() && !isSomeAnimationInProgress() && rectButton.contains(event.getX(), event.getY())) {
                    isCancelled = false;
                    pointerDownTime = System.currentTimeMillis();
                    onTouchInternal();
                    return true;
                }
                return false;

            case MotionEvent.ACTION_MOVE:
                if (isCancelled) {
                    return false;
                }
                if (!rectButton.contains(event.getX(), event.getY())) {
                    isCancelled = true;
                    onCancelInternal(true, shouldTriggerListener());
                    return false;
                }
                return true;

            case MotionEvent.ACTION_UP:
                if (!isCancelled) {
                    onCancelInternal(false, shouldTriggerListener());
                    return true;
                }
                return false;
        }
        return false;
    }

    private boolean shouldTriggerListener() {
//        return System.currentTimeMillis() - pointerDownTime >= singlePressThreshold;
        return true;
    }

    private void onTouchInternal() {
        if (controllerActionListener != null) {
            controllerActionListener.onFirstTouch();
        }
        boolean tmpLongTapAvailable = longTapEnabled;
        cancelAnimator(pressAnimator);
        if (pressAnimator == null) {
            pressAnimator = ValueAnimator.ofFloat(0f, 1f);
            pressAnimator.addUpdateListener(pressExpandListener);
        }
        pressAnimator.setDuration(singlePressDuration);
        pressAnimator.setInterpolator(PRESS_EXPAND_INTERPOLATOR);
        pressAnimator.removeAllListeners();
        pressAnimator.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                currentState = STATE_PRESSED;
                vibrateOnTouch();
                if (tmpLongTapAvailable) {
                    onLongPressInternal();
                }
            }
        });

        pressAnimator.start();
    }

    private void onCancelInternal(final boolean isCancelled, final boolean shouldTriggerListener) {
        if (currentState == STATE_NOT_PRESSED && shouldTriggerListener && !isCancelled) {
            vibrateOnTouch();
        }
        if (currentState == STATE_NOT_PRESSED || currentState == STATE_PRESSED) {
            if (controllerActionListener != null) {
                if (shouldTriggerListener) {
                    if (isCancelled) {
                        controllerActionListener.onCancel(this);
                    } else {
                        controllerActionListener.onSinglePress(this);
                    }
                } else {
                    controllerActionListener.onFinalRelease();
                }
            }
            if (isAnimatorRunning(longPressAnimator)) {
                longPressAnimator.setInterpolator(LONG_PRESS_COLLAPSE_INTERPOLATOR);
                longPressAnimator.removeAllListeners();
                longPressAnimator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (pressAnimator != null) {
                            pressAnimator.setInterpolator(PRESS_COLLAPSE_INTERPOLATOR);

                            pressAnimator.removeAllListeners();
                            pressAnimator.addListener(new AnimatorListenerAdapter() {

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    if (controllerActionListener != null) {
                                        controllerActionListener.onFinalRelease();
                                    }
                                    currentState = STATE_NOT_PRESSED;
                                }
                            });
                            pressAnimator.reverse();
                        }
                    }
                });
                longPressAnimator.reverse();
            } else {
                if (pressAnimator != null) {
                    pressAnimator.setInterpolator(PRESS_COLLAPSE_INTERPOLATOR);

                    pressAnimator.removeAllListeners();
                    pressAnimator.addListener(new AnimatorListenerAdapter() {

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            currentState = STATE_NOT_PRESSED;
                        }
                    });

                    pressAnimator.reverse();
                }
            }
        } else if (currentState == STATE_LONG_PRESSED) {
            if (controllerActionListener != null) {
                if (isCancelled) {
                    controllerActionListener.onCancel(this);
                } else {
                    controllerActionListener.onLongPressPreRelease(this);
                }
            }
            if (longPressAnimator != null) {
                longPressAnimator.setInterpolator(LONG_PRESS_COLLAPSE_INTERPOLATOR);
                longPressAnimator.removeAllListeners();
                longPressAnimator.setDuration(ANIMATION_COLLAPSE_DURATION_MILLIS);
                longPressAnimator.addListener(new AnimatorListenerAdapter() {

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (pressAnimator != null) {
                            pressAnimator.setInterpolator(new AccelerateInterpolator(4f));

                            pressAnimator.removeAllListeners();
                            pressAnimator.addListener(new AnimatorListenerAdapter() {

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    if (controllerActionListener != null) {
                                        if (isCancelled) {
                                            controllerActionListener.onCancel(CameraRecordControlButton.this);
                                        } else {
                                            controllerActionListener.onLongPressRelease(CameraRecordControlButton.this);
                                            controllerActionListener.onFinalRelease();
                                        }
                                    }
                                    currentState = STATE_NOT_PRESSED;
                                }
                            });

                            pressAnimator.reverse();
                        }
                    }
                });
                longPressAnimator.reverse();
            }
        }
    }

    private void onLongPressInternal() {
        cancelAnimator(longPressAnimator);
        if (longPressAnimator == null) {
            longPressAnimator = ValueAnimator.ofFloat(0f, 1f);
            longPressAnimator.addUpdateListener(longPressExpandListener);
        }
        longPressAnimator.setDuration(longPressDuration);
        longPressAnimator.setInterpolator(LONG_PRESS_EXPAND_INTERPOLATOR);
        longPressAnimator.removeAllListeners();
        longPressAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                currentState = STATE_LONG_PRESSED;
                vibrateOnTouch();
                if (controllerActionListener != null) {
                    controllerActionListener.onLongPress(CameraRecordControlButton.this);
                }
            }
        });

        longPressAnimator.start();
    }

    public void setControlListener(@NonNull ControllerActionListener listener) {
        this.controllerActionListener = listener;
    }

    private void cancelAnimator(@Nullable ValueAnimator animator) {
        if (animator != null) {
            animator.cancel();
        }
    }

    private class LongPressExpandListener extends BaseLongPressAnimUpdateListener {

        LongPressExpandListener(
                float progressWidthAfterPress,
                float progressWidthAfterLongPress,
                float progressRadiusAfterPress,
                float progressRadiusAfterLongPress,
                RectF originalProgressBackgroundRect) {
            super(progressWidthAfterPress, progressWidthAfterLongPress, progressRadiusAfterPress,
                    progressRadiusAfterLongPress, originalProgressBackgroundRect);
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            float percent = (float) animation.getAnimatedValue();
            paintProgressBackground.setStrokeWidth(progressWidthAfterPress + progressWidthAdd * percent);

            float progressAdd = progressRadiusAdd * percent;
            rectProgressBackground.set(
                    progressRectAfterPress.left - progressAdd,
                    progressRectAfterPress.top - progressAdd,
                    progressRectAfterPress.right + progressAdd,
                    progressRectAfterPress.bottom + progressAdd
            );
            postInvalidateOnAnimation();
        }
    }

    private class PressExpandListener extends BasePressAnimUpdateListener {

        PressExpandListener(
                float originalProgressWidth,
                float progressWidthAfterPress,
                float buttonRadiusAfterPress,
                float progressRadiusAfterPress,
                RectF originalProgressBackgroundRect,
                RectF originalButtonRect) {
            super(originalProgressWidth, progressWidthAfterPress, buttonRadiusAfterPress,
                    progressRadiusAfterPress, originalProgressBackgroundRect, originalButtonRect);
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            float percent = (float) animation.getAnimatedValue();
            paintProgressBackground.setStrokeWidth(originalProgressWidth + progressWidthAdd * percent);

            float progressAdd = progressRadiusAdd * percent;
            rectProgressBackground.set(
                    originalProgressBackgroundRect.left - progressAdd,
                    originalProgressBackgroundRect.top - progressAdd,
                    originalProgressBackgroundRect.right + progressAdd,
                    originalProgressBackgroundRect.bottom + progressAdd
            );

            float buttonAdd = buttonRadiusAdd * percent;
            buttonRadius = Math.round(originalButtonRadius + buttonAdd);
            rectButton.set(
                    originalButtonRect.left - buttonAdd,
                    originalButtonRect.top - buttonAdd,
                    originalButtonRect.right + buttonAdd,
                    originalButtonRect.bottom + buttonAdd
            );

            postInvalidateOnAnimation();
        }
    }

    private void vibrateOnTouch() {
        if (isHapticFeedbackEnabled()) {
            performHapticFeedback(HapticFeedbackConstants.LONG_PRESS, HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);
        }
    }

    private static float dp2px_f(int dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, Resources.getSystem().getDisplayMetrics());
    }

    private boolean isSomeAnimationInProgress() {
        return isAnimatorRunning(pressAnimator) || isAnimatorRunning(longPressAnimator);
    }

    private boolean isAnimatorRunning(Animator animator) {
        return animator != null && animator.isRunning();
    }

    public RectF getContentRect() {
        return new RectF(rectProgressBackground);
    }

    public void setLongTapEnabled(boolean enabled) {
        longTapEnabled = enabled;
    }

    public boolean isLongTapEnabled() {
        return longTapEnabled;
    }
}