package com.mgrmobi.intouch.cameramodule.camera.ui.button;

import android.animation.ValueAnimator;
import android.graphics.RectF;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public abstract class BasePressAnimUpdateListener implements ValueAnimator.AnimatorUpdateListener {
    protected final float originalProgressWidth;
    protected final float originalButtonRadius;
    protected final float progressWidthAdd;
    protected final float progressRadiusAdd;
    protected final float buttonRadiusAdd;
    protected final RectF originalProgressBackgroundRect;
    protected final RectF originalButtonRect;


    protected BasePressAnimUpdateListener(
            float originalProgressWidth,
            float progressWidthAfterPress,
            float buttonRadiusAfterPress,
            float progressRadiusAfterPress,
            RectF originalProgressBackgroundRect,
            RectF originalButtonRect) {
        this.originalProgressWidth = originalProgressWidth;
        this.originalButtonRadius = originalButtonRect.width() / 2;
        this.originalProgressBackgroundRect = originalProgressBackgroundRect;
        this.originalButtonRect = originalButtonRect;

        progressWidthAdd = progressWidthAfterPress - originalProgressWidth;
        progressRadiusAdd = progressRadiusAfterPress - originalProgressBackgroundRect.width() / 2;
        buttonRadiusAdd = buttonRadiusAfterPress - originalButtonRadius;
    }
}
