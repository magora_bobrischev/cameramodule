package com.mgrmobi.intouch.cameramodule.editor.ui.colorSelection;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import intouch.mgrmobi.com.intouchcameramodule.R;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */
public class HolderColorItem extends RecyclerView.ViewHolder {
    @NonNull private final AdapterSimpleColorSelector.ItemSelectionCallback callback;
    final ColorItem colorImage;

    private HolderColorItem(View itemView, @NonNull AdapterSimpleColorSelector.ItemSelectionCallback callback) {
        super(itemView);
        colorImage = (ColorItem) itemView.findViewById(R.id.color_item);
        this.callback = callback;
    }

    public void bind(@NonNull ColorData data) {
        colorImage.setColor(data.color);
        colorImage.setSelected(data.isChecked);

        colorImage.setOnClickListener(v -> callback.onItemSelected(data));
    }

    static HolderColorItem createHolder(ViewGroup parent, @NonNull AdapterSimpleColorSelector.ItemSelectionCallback callback) {
        return new HolderColorItem(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.holder_color_item, parent, false), callback);
    }
}
