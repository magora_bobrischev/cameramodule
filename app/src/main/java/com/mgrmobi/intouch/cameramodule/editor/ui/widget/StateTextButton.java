package com.mgrmobi.intouch.cameramodule.editor.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.IntDef;
import android.support.annotation.StringRes;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import intouch.mgrmobi.com.intouchcameramodule.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public class StateTextButton extends AppCompatButton {
    public static final int STATE_DEFAULT = 0;
    public static final int STATE_DRAWING = 1;
    public static final int STATE_EMOJIES = 2;
    @StringRes private final int textDefault;
    @StringRes private final int textDrawing;
    @StringRes private final int textEmojies;

    @IntDef({STATE_DEFAULT, STATE_DRAWING, STATE_EMOJIES})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ButtonTextStates {

    }

    public StateTextButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StateTextButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null) {
            TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.StateTextButton, defStyleAttr, 0);
            try {
                textDefault = array.getResourceId(R.styleable.StateTextButton_stb_textDefault, -1);
                textDrawing = array.getResourceId(R.styleable.StateTextButton_stb_textDrawing, -1);
                textEmojies = array.getResourceId(R.styleable.StateTextButton_stb_textEmojies, -1);

                setState(STATE_DEFAULT);
            } finally {
                array.recycle();
            }
        } else {
            throw new IllegalArgumentException("Attrs must not be null");
        }
    }

    public void setState(@ButtonTextStates int state) {
        switch (state) {
            case STATE_DRAWING:
                setText(textDrawing);
                break;

            case STATE_EMOJIES:
                setText(textEmojies);
                break;

            case STATE_DEFAULT:
            default:
                setText(textDefault);
                break;
        }
    }
}
