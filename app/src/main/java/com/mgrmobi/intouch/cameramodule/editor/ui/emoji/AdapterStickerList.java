package com.mgrmobi.intouch.cameramodule.editor.ui.emoji;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import intouch.mgrmobi.com.intouchcameramodule.R;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public class AdapterStickerList extends RecyclerView.Adapter<AdapterStickerList.StickerHolder> {
    private final TypedArray emojies;
    private final OnStickerClickListener onStickerClickListener;

    public interface OnStickerClickListener {

        void onStickerClicked(@DrawableRes int resourceId);
    }

    public AdapterStickerList(@NonNull Resources resources, @NonNull OnStickerClickListener onStickerClickListener) {
//        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "Resources.txt");
//
//        try {
//            if (!file.exists()) {
//                file.createNewFile();
//            }else{
//                file.delete();
//                file.createNewFile();
//            }
//            FileWriter fv = new FileWriter(file);
//
//            for (int identifier = (R.drawable.emoji_0__u1f441_200d_1f5e8); identifier <= (R.drawable.emoji_9_u2615); identifier++) {
//                if (resources.getResourceEntryName(identifier).startsWith("emoji_")) {
//                    fv.write("<item>@drawable/" + resources.getResourceEntryName(identifier) + "</item>\n");
//                }
//            }
//
//            fv.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        emojies = resources.obtainTypedArray(R.array.Emojis);
        this.onStickerClickListener = onStickerClickListener;
    }

    @Override
    public StickerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        StickerHolder holder = StickerHolder.createHolder(parent);
        holder.imageEmoji.setOnClickListener(v -> {
            int adapterPosition = holder.getAdapterPosition();
            if (adapterPosition != RecyclerView.NO_POSITION) {
                onStickerClickListener.onStickerClicked(emojies.getResourceId(adapterPosition, -1));
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(StickerHolder holder, int position) {
        holder.bind(emojies.getResourceId(position, -1));
    }

    @Override
    public int getItemCount() {
        return emojies.length();
    }

    static class StickerHolder extends RecyclerView.ViewHolder {
        final ImageView imageEmoji;

        private StickerHolder(View itemView) {
            super(itemView);
            imageEmoji = (ImageView) itemView.findViewById(R.id.image_emoji);
        }

        void bind(@DrawableRes int resId) {
            imageEmoji.setImageResource(resId);
        }

        static StickerHolder createHolder(ViewGroup parent) {
            return new StickerHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_sticker_item, parent, false));
        }
    }
}
