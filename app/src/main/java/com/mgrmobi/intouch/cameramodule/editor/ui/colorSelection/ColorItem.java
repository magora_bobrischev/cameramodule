package com.mgrmobi.intouch.cameramodule.editor.ui.colorSelection;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.ColorInt;
import android.support.annotation.Keep;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import com.mgrmobi.intouch.cameramodule.editor.utils.UiHelper;
import intouch.mgrmobi.com.intouchcameramodule.R;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public class ColorItem extends AppCompatImageView {
    private final Paint colorPaint;
    private final int borderColor;
    private final int borderWidth;
    private final float checkedScale;

    @ColorInt private int color = Color.TRANSPARENT;
    private int defaultRadius;
    private int currentRadius;
    private int currentBorderWidth;

    private boolean isSelected;

    public ColorItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = null;
        try {
            array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ColorItem, 0, 0);
            borderColor = array.getColor(R.styleable.ColorItem_ci_borderColor, Color.WHITE);
            borderWidth = array.getDimensionPixelSize(R.styleable.ColorItem_ci_borderWidth, UiHelper.dp2px_i(2));
            checkedScale = array.getFloat(R.styleable.ColorItem_ci_selectedScale, 1.3f);

            colorPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            currentBorderWidth = borderWidth;
        } finally {
            if (array != null) {
                array.recycle();
            }
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        currentRadius = Math.min(w, h) / 2 - borderWidth;
        defaultRadius = currentRadius;
    }

    public void setColor(@ColorInt int color) {
        this.color = color;
        postInvalidateOnAnimation();
    }

    public void setSelected(boolean selected) {
        if (isSelected != selected) {
            isSelected = selected;
        }
    }

    public boolean isSelected() {
        return isSelected;
    }

    @Keep
    public void setCurrentRadius(int radius) {
        currentRadius = radius;
        invalidate();
    }

    @Keep
    public int getCurrentRadius() {
        return currentRadius;
    }

    public int getMaximumRadius() {
        return Math.round(defaultRadius * checkedScale);
    }

    public int getInitialRadius() {
        return defaultRadius;
    }

    public int getInitialBorderWidth() {
        return borderWidth;
    }

    @Keep
    public void setBorderWidth(int borderWidth) {
        this.currentBorderWidth = borderWidth;
    }

    @Keep
    public int getBorderWidth() {
        return currentBorderWidth;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (color == Color.TRANSPARENT) {
            super.onDraw(canvas);
            return;
        }
        Paint tmpPaint = colorPaint;
        int radius = currentRadius;
        int cx = canvas.getWidth() / 2;
        int cy = canvas.getHeight() / 2;

        int bwidth = currentBorderWidth;
        if (bwidth != 0) {
            tmpPaint.setStyle(Paint.Style.STROKE);
            tmpPaint.setStrokeWidth(bwidth);
            tmpPaint.setColor(borderColor);

            canvas.drawCircle(
                    cx,
                    cy,
                    radius,
                    tmpPaint
            );
        }
        tmpPaint.setStyle(Paint.Style.FILL);
        tmpPaint.setColor(color);

        canvas.drawCircle(
                cx,
                cy,
                radius,
                tmpPaint
        );
    }
}
