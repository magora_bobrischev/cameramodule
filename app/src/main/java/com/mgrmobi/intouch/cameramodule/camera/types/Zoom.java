package com.mgrmobi.intouch.cameramodule.camera.types;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.mgrmobi.intouch.cameramodule.camera.constants.CameraKit.Constants.ZOOM_OFF;
import static com.mgrmobi.intouch.cameramodule.camera.constants.CameraKit.Constants.ZOOM_PINCH;


@Retention(RetentionPolicy.SOURCE)
@IntDef({ZOOM_OFF, ZOOM_PINCH})
public @interface Zoom {
}
