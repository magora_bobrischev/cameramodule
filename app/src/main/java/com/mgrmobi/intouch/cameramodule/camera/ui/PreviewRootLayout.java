package com.mgrmobi.intouch.cameramodule.camera.ui;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import com.mgrmobi.intouch.cameramodule.editor.ui.emoji.AdapterStickerList;
import com.mgrmobi.intouch.cameramodule.editor.ui.emoji.GridSpacingDecoration;
import com.mgrmobi.intouch.cameramodule.editor.utils.UiHelper;
import com.mgrmobi.intouch.cameramodule.editor.widget.EmojiesBlurLinearLayout;
import intouch.mgrmobi.com.intouchcameramodule.R;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */
public class PreviewRootLayout extends CoordinatorLayout {
    private static final int H;
    private static final int HIDE_EMOJIES_LIST_SPEED_THRESHOLD;
    private static final int EMOJIES_LIST_SPANS = 6;
    private ViewDragHelper vdh;
    private RecyclerView emojisList;
    private EmojiesBlurLinearLayout emojisListHolder;
    private View emojiesListHeader;
    private EmojiListListener emojiListListener;
    private AdapterStickerList.OnStickerClickListener onStickerClickListener;
    private Transition emojiListAppearing;

    public interface EmojiListListener {
        void onEmojiesListVisible();

        void onEmojiesListHidden();
    }

    static {
        H = Resources.getSystem().getDisplayMetrics().heightPixels;
        HIDE_EMOJIES_LIST_SPEED_THRESHOLD = 5500;
    }

    public PreviewRootLayout(Context context) {
        super(context);
    }

    public PreviewRootLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PreviewRootLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        emojisList = (RecyclerView) findViewById(R.id.emoji_list);
        emojisList.setLayoutManager(new GridLayoutManager(getContext(), EMOJIES_LIST_SPANS, GridLayoutManager.VERTICAL, false));
        emojisList.addItemDecoration(new GridSpacingDecoration(EMOJIES_LIST_SPANS, UiHelper.dp2px_i(16), UiHelper.dp2px_i(16), true));

        emojisListHolder = (EmojiesBlurLinearLayout) findViewById(R.id.emojies_list_holder);
        emojiesListHeader = findViewById(R.id.emojies_list_header);
    }

    @Override
    public void computeScroll() {
        super.computeScroll();
        if (vdh != null && vdh.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    public void showEmojiesList() {
        boolean firstLayout = emojisList.getAdapter() == null;
        if (emojisList.getAdapter() == null) {
            emojisList.setAdapter(new AdapterStickerList(getResources(), onStickerClickListener));
        }
        ensureDragHelper();
        ensureEmojiesListTransition();

        emojisListHolder.setVisibility(INVISIBLE);
        emojisListHolder.post(() -> {
            if (firstLayout) {
                ViewCompat.offsetTopAndBottom(emojisListHolder, H - emojisListHolder.getMeasuredHeight());
                TransitionManager.beginDelayedTransition(this, emojiListAppearing);
                emojisListHolder.setVisibility(VISIBLE);
                emojisListHolder.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        emojisListHolder.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        ViewCompat.offsetTopAndBottom(emojisListHolder, H - emojisListHolder.getMeasuredHeight());
                    }
                });
            } else {
                ViewCompat.offsetTopAndBottom(emojisListHolder, H - emojisListHolder.getMeasuredHeight());
                TransitionManager.beginDelayedTransition(this, emojiListAppearing);
                emojisListHolder.setVisibility(VISIBLE);
            }
            if (emojiListListener != null) {
                emojiListListener.onEmojiesListVisible();
            }
        });
    }

    private void ensureDragHelper() {
        if (vdh != null) {
            return;
        }
        vdh = ViewDragHelper.create(this, 1.0f, new ViewDragHelper.Callback() {

            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                return child.getId() == R.id.emojies_list_holder && (!emojisList.canScrollVertically(-1) || isScrollingHeaderTouched);
            }

            @Override
            public int clampViewPositionVertical(View child, int top, int dy) {
                int topBound = H - emojisListHolder.getMeasuredHeight();
                int bottomBound = getHeight() - getPaddingBottom();

                return Math.min(Math.max(top, topBound), bottomBound);
            }

            @Override
            public void onViewReleased(View releasedChild, float xvel, float yvel) {
                if (yvel > HIDE_EMOJIES_LIST_SPEED_THRESHOLD && (!emojisList.canScrollVertically(-1) || isScrollingHeaderTouched)) {
                    hideEmojiesList();
                } else if (releasedChild.getTop() > (H - emojisListHolder.getMeasuredHeight() / 2)) {
                    hideEmojiesList();
                } else {
                    if (vdh.settleCapturedViewAt(0, H - emojisListHolder.getMeasuredHeight())) {
                        ViewCompat.postInvalidateOnAnimation(PreviewRootLayout.this);
                    }
                }
            }

            @Override
            public int getViewVerticalDragRange(View child) {
                return H - emojisListHolder.getMeasuredHeight();
            }
        });
    }

    public boolean hideEmojiesList(boolean withAnimation) {
        if (emojisListHolder != null && emojisListHolder.getVisibility() == VISIBLE) {
            if (withAnimation) {
                ensureDragHelper();
                ensureEmojiesListTransition();

                TransitionManager.beginDelayedTransition(this, emojiListAppearing);
            }
            emojisListHolder.setVisibility(GONE);
            if (emojiListListener != null) {
                emojiListListener.onEmojiesListHidden();
            }
            return true;
        }
        return false;
    }

    public boolean hideEmojiesList() {
        return hideEmojiesList(true);
    }

    private void ensureEmojiesListTransition() {
        if (emojiListAppearing == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                emojiListAppearing = new Slide(Gravity.BOTTOM).addTarget(emojisListHolder);
            } else {
                emojiListAppearing = new Fade().addTarget(emojisListHolder);
            }
        }
    }

    public void showOrHideEmojiesList() {
        if (emojisListHolder != null && emojisListHolder.getVisibility() == View.VISIBLE) {
            hideEmojiesList();
        } else {
            showEmojiesList();
        }
    }

    private boolean isScrollingHeaderTouched;

    private boolean isScrollingHeaderViewUnderViewUnder(int x, int y) {
        if (emojisListHolder == null)
            return false;

        int[] viewLocation = new int[2];
        emojisListHolder.getLocationOnScreen(viewLocation);
        int[] parentLocation = new int[2];
        this.getLocationOnScreen(parentLocation);
        int screenX = parentLocation[0] + x;
        int screenY = parentLocation[1] + y;
        return screenX >= viewLocation[0] &&
                screenX < viewLocation[0] + emojisListHolder.getWidth() &&
                screenY >= viewLocation[1] &&
                screenY < viewLocation[1] + emojiesListHeader.getMeasuredHeight();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (vdh != null && emojisListHolder != null && emojisListHolder.getVisibility() == VISIBLE) {
            int action = MotionEventCompat.getActionMasked(ev);
            if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
                vdh.cancel();
                return false;
            }
            isScrollingHeaderTouched = isScrollingHeaderViewUnderViewUnder((int) ev.getX(), (int) ev.getY());
            return vdh.shouldInterceptTouchEvent(ev);
        } else {
            return super.onInterceptTouchEvent(ev);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (vdh != null) {
            vdh.processTouchEvent(ev);
            return true;
        }
        return super.onTouchEvent(ev);
    }

    public void setEmojiesListListener(@Nullable EmojiListListener listListener) {
        this.emojiListListener = listListener;
    }

    public void setOnStickerClickListener(@NonNull AdapterStickerList.OnStickerClickListener onStickerClickListener) {
        this.onStickerClickListener = onStickerClickListener;
    }
}
