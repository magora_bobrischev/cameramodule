package com.mgrmobi.intouch.cameramodule.editor.ui.colorSelection;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.ColorLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import com.mgrmobi.intouch.cameramodule.editor.utils.UiHelper;
import intouch.mgrmobi.com.intouchcameramodule.R;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */
public class WidgetSimpleColorSelector extends RecyclerView {
    private static final int OFFSET_START_END_PX = UiHelper.dp2px_i(18);
    private static final int OFFSET_BETWEEN_PX = UiHelper.dp2px_i(20);
    private ColorData selectedColorData;
    private ColorSelectionListener colorSelectionListener;

    public interface ColorSelectionListener {

        void onColorSelected(@ColorInt int color);
    }

    public WidgetSimpleColorSelector(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.WidgetSimpleColorSelector, 0, 0);
        int[] colors;
        int resId = array.getResourceId(R.styleable.WidgetSimpleColorSelector_wscs_colors, -1);
        if (resId != -1) {
            colors = context.getResources().getIntArray(resId);
        } else {
            colors = new int[]{Color.WHITE, Color.BLACK};
        }
        array.recycle();

        setLayoutManager(new ColorLayoutManager(context, LinearLayoutManager.HORIZONTAL, false, OFFSET_START_END_PX, OFFSET_BETWEEN_PX));
        addItemDecoration(new ColorsItemDecoration(OFFSET_START_END_PX, OFFSET_BETWEEN_PX));
        setItemAnimator(new ColorCheckItemAnimator());

        AdapterSimpleColorSelector adapter = new AdapterSimpleColorSelector(colors, item -> {
            selectedColorData = item;
            if (colorSelectionListener != null) {
                colorSelectionListener.onColorSelected(item.color);
            }
        });
        setAdapter(adapter);

        post(() -> adapter.selectColor(selectedColorData));
    }

    public void setColorSelectionListener(@Nullable ColorSelectionListener listener) {
        colorSelectionListener = listener;
    }

    public void setInitialColor(@ColorInt int color) {
        selectedColorData = ColorData.of(color);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Bundle state = new Bundle();
        state.putParcelable("COLOR", selectedColorData);
        state.putParcelable("PARENT_STATE", super.onSaveInstanceState());
        return state;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle restoredState = (Bundle) state;
            if (selectedColorData == null) {
                selectedColorData = restoredState.getParcelable("COLOR");
            }
            super.onRestoreInstanceState(restoredState.getParcelable("PARENT_STATE"));
        } else {
            super.onRestoreInstanceState(state);
        }
    }

    @ColorInt
    public int getSelectedColor() {
        return selectedColorData != null ? selectedColorData.color : Color.WHITE;
    }
}
