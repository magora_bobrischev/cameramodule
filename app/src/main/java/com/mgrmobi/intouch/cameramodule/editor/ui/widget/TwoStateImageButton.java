package com.mgrmobi.intouch.cameramodule.editor.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;
import android.view.View;
import intouch.mgrmobi.com.intouchcameramodule.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public class TwoStateImageButton extends AppCompatImageButton {
    public static final int STATE_DEFAULT = 0;
    public static final int STATE_ALTERNATIVE = 1;

    @DrawableRes private final int icDefault;
    @DrawableRes private final int icAlternative;

    private StateClickListener stateClickListener;
    private int currentState;

    @IntDef({STATE_DEFAULT, STATE_ALTERNATIVE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TwoStateImageButtonStates {

    }

    public interface StateClickListener {
        void onDefaultStateClicked(View v);

        void onAlternateStateClicked(View v);
    }

    public TwoStateImageButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TwoStateImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TwoStateImageButton, defStyleAttr, 0);
        try {
            icDefault = array.getResourceId(R.styleable.TwoStateImageButton_tsib_icDefault, -1);
            icAlternative = array.getResourceId(R.styleable.TwoStateImageButton_tsib_icAlternate, -1);
            currentState = icDefault;

            setImageResource(icDefault);

            setOnClickListener(v -> {
                if (stateClickListener != null) {
                    if (currentState == icDefault) {
                        stateClickListener.onDefaultStateClicked(TwoStateImageButton.this);
                    } else {
                        stateClickListener.onAlternateStateClicked(TwoStateImageButton.this);
                    }
                }
            });
        } finally {
            array.recycle();
        }
    }

    public void setState(@TwoStateImageButtonStates int state) {
        switch (state) {
            case STATE_ALTERNATIVE:
                setImageResource(icAlternative);
                currentState = icAlternative;
                break;

            case STATE_DEFAULT:
            default:
                setImageResource(icDefault);
                currentState = icDefault;
                break;
        }
    }

    public void setStateClickListener(@Nullable StateClickListener clickListener) {
        stateClickListener = clickListener;
    }
}
