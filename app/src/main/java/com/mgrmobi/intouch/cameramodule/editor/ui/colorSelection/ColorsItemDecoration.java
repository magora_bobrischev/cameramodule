package com.mgrmobi.intouch.cameramodule.editor.ui.colorSelection;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public class ColorsItemDecoration extends RecyclerView.ItemDecoration {
    private final int offsetStartEnd;
    private final int offsetBetween;

    public ColorsItemDecoration(int offsetStartEnd, int offsetBetween) {
        this.offsetStartEnd = offsetStartEnd;
        this.offsetBetween = offsetBetween / 2;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int childAdapterPosition = parent.getChildAdapterPosition(view);
        boolean isFirst = childAdapterPosition == 0;
        boolean isLast = childAdapterPosition == parent.getAdapter().getItemCount() - 1;
        if (isFirst && isLast) {
            outRect.left = offsetStartEnd;
            outRect.right = offsetStartEnd;
        } else if (isFirst) {
            outRect.left = offsetStartEnd;
            outRect.right = offsetBetween;
        } else if (isLast) {
            outRect.left = offsetBetween;
            outRect.right = offsetStartEnd;
        } else {
            outRect.left = offsetBetween;
            outRect.right = offsetBetween;
        }
    }
}
