package com.mgrmobi.intouch.cameramodule.camera.utils.filename;

import android.os.Environment;
import android.support.annotation.Nullable;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */

public class DefaultFileNameProvider implements FileNameProvider {

    @Nullable
    @Override
    public File getVideoFile() {
        File directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                return null;
            }
        }
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss", Locale.getDefault());
        return new File(directory, "showapp_video_" + format.format(System.currentTimeMillis()) + ".mp4");
    }

    @Nullable
    @Override
    public String getVideoFilePath() {
        File videoFile = getVideoFile();
        return videoFile != null ? videoFile.getPath() : null;
    }

    @Nullable
    @Override
    public File getImageFile() {
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsoluteFile() + "/Showapp/");
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                return null;
            }
        }
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss", Locale.getDefault());
        return new File(directory, "showapp_photo_" + format.format(System.currentTimeMillis()) + ".jpg");
    }

    @Nullable
    @Override
    public String getImageFilePath() {
        File imageFile = getImageFile();
        return imageFile != null ? imageFile.getPath() : null;
    }
}
