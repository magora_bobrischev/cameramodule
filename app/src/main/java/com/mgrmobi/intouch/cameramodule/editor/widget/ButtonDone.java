package com.mgrmobi.intouch.cameramodule.editor.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.HapticFeedbackConstants;
import android.view.View;
import com.mgrmobi.intouch.cameramodule.editor.utils.UiHelper;
import intouch.mgrmobi.com.intouchcameramodule.R;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public class ButtonDone extends FloatingActionButton {
    @DrawableRes private static final int DEFAULT_RES_DONE = R.drawable.ic_button_done_accent_24dp;
    @DrawableRes private static final int DEFAULT_RES_REMOVE = R.drawable.ic_button_remove_item;
    private static final int DEFAULT_ELEVATION_DP = 6;
    private static final float DEFAULT_REMOVE_STATE_SCALE = 1.5f;
    private static final int DEFAULT_FLIP_DURATION_MILLIS = 300;
    private static final int STATE_DONE = 1;
    private static final int STATE_REMOVE = 2;

    private AnimatorSet setFlipToRemove;
    private AnimatorSet setFlipToDone;
    private int currentState = STATE_DONE;
    @DrawableRes private int resIcDone;
    @DrawableRes private int resIcRemove;
    @ColorInt private int colorBackgroundDone;
    @ColorInt private int colorBackgroundRemove;
    private int flipDuration;
    private float removeStateScale;
    private boolean preparedToRemove;

    public ButtonDone(Context context) {
        super(context);
        init(context, null, 0);
    }

    public ButtonDone(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public ButtonDone(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        if (attrs != null) {
            TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ButtonDone, defStyleAttr, 0);
            resIcDone = array.getResourceId(R.styleable.ButtonDone_bd_doneIcon, DEFAULT_RES_DONE);
            resIcRemove = array.getResourceId(R.styleable.ButtonDone_bd_removeIcon, DEFAULT_RES_REMOVE);
            colorBackgroundDone = array.getColor(R.styleable.ButtonDone_bd_doneBackgroundColor, Color.WHITE);
            colorBackgroundRemove = array.getColor(R.styleable.ButtonDone_bd_removeBackgroundColor, Color.BLACK);
            flipDuration = array.getInt(R.styleable.ButtonDone_bd_flipDuration, DEFAULT_FLIP_DURATION_MILLIS);
            removeStateScale = array.getFloat(R.styleable.ButtonDone_bd_removeStateScale, DEFAULT_REMOVE_STATE_SCALE);
            array.recycle();
        } else {
            resIcDone = DEFAULT_RES_DONE;
            resIcRemove = DEFAULT_RES_REMOVE;
            colorBackgroundDone = Color.WHITE;
            colorBackgroundRemove = Color.BLACK;
            flipDuration = DEFAULT_FLIP_DURATION_MILLIS;
        }
    }

    private void ensureAnimators() {
        if (setFlipToRemove == null) {
            setFlipToRemove = new AnimatorSet();
            ObjectAnimator rotate1 = ObjectAnimator.ofFloat(this, View.ROTATION_Y, 90).setDuration(flipDuration / 2);
            rotate1.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationCancel(Animator animation) {
                    setImageResource(resIcRemove);
                    setBackgroundTintList(ColorStateList.valueOf(colorBackgroundRemove));
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    setImageResource(resIcRemove);
                    setBackgroundTintList(ColorStateList.valueOf(colorBackgroundRemove));
                }
            });
            AnimatorSet set1 = new AnimatorSet();
            set1.playSequentially(
                    rotate1,
                    ObjectAnimator.ofFloat(this, View.ROTATION_Y, 0).setDuration(flipDuration / 2)
            );
            setFlipToRemove.playTogether(
                    set1,
                    ObjectAnimator.ofFloat(this, View.SCALE_X, getScaleX(), removeStateScale).setDuration(flipDuration),
                    ObjectAnimator.ofFloat(this, View.SCALE_Y, getScaleY(), removeStateScale).setDuration(flipDuration)
            );
            setFlipToRemove.setInterpolator(new FastOutSlowInInterpolator());
        }
        if (setFlipToDone == null) {
            setFlipToDone = new AnimatorSet();
            ObjectAnimator rotate1 = ObjectAnimator.ofFloat(this, View.ROTATION_Y, 90).setDuration(flipDuration / 2);
            rotate1.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationCancel(Animator animation) {
                    setImageResource(resIcDone);
                    setBackgroundTintList(ColorStateList.valueOf(colorBackgroundDone));
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    setImageResource(resIcDone);
                    setBackgroundTintList(ColorStateList.valueOf(colorBackgroundDone));
                }
            });
            AnimatorSet set1 = new AnimatorSet();
            set1.playSequentially(
                    rotate1,
                    ObjectAnimator.ofFloat(this, View.ROTATION_Y, 0).setDuration(flipDuration / 2)
            );
            setFlipToDone.playTogether(
                    ObjectAnimator.ofFloat(this, View.SCALE_X, getScaleX(), 1).setDuration(flipDuration),
                    ObjectAnimator.ofFloat(this, View.SCALE_Y, getScaleY(), 1).setDuration(flipDuration),
                    set1
            );

            setFlipToDone.setInterpolator(new FastOutSlowInInterpolator());
            setFlipToDone.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationCancel(Animator animation) {
                    UiHelper.elevateDp(ButtonDone.this, DEFAULT_ELEVATION_DP);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    UiHelper.elevateDp(ButtonDone.this, DEFAULT_ELEVATION_DP);
                }
            });
        }
    }

    public void switchToRemove() {
        ensureAnimators();

        if (currentState == STATE_DONE) {
            currentState = STATE_REMOVE;

            setFlipToDone.cancel();

            ViewCompat.setElevation(this, 0);

            setFlipToRemove.start();
        }
    }

    public void switchToDone() {
        ensureAnimators();

        if (currentState == STATE_REMOVE) {
            currentState = STATE_DONE;

            setFlipToRemove.cancel();
            setFlipToDone.start();

            preparedToRemove = false;
        }
    }

    public void setPreparedToRemove(boolean prepared) {
        if (preparedToRemove != prepared) {
            preparedToRemove = prepared;
            makeVibration();
        }
    }

    private void makeVibration() {
        if (isHapticFeedbackEnabled()) {
            performHapticFeedback(HapticFeedbackConstants.LONG_PRESS, HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);
        }
    }
}
